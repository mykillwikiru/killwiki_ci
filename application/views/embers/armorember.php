<div class="panel panel-info">
    <div class="panel-heading">
        <div class="sidebar-header">
            <a href="/embers" class="btn btn-lg btn-warning">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"> Embers</span>
            </a>
        </div>
    </div>
        <div class="panel-body mainitemmod">
            <div class="row">
                
                <!-- block sitebaritem смещается вверх при меньших разрешениях-->
                <div class="col-lg-4 col-lg-push-8">
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header"><?php echo $name; ?></div></div>
                            <div class="panel-body itemtable">
                                <img class="img-responsive" src="<?php echo $img ?>">
                                <?php foreach ($tableinarmor as $armor): ?>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td class="leftinfo">Тип:</td>
                                            <td><?php echo $armor->type; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="leftinfo">Зачарование:</td>
                                            <td><?php echo $armor->enchant; ?></td>
                                        </tr>
                                    </table>
                                <?php endforeach ?>
                            </div>
                    </div>
                </div>
                <!-- contentitem смещается вниз при меньших разрешениях-->
                <div class="col-lg-8 col-lg-pull-4">
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Описание предмета <?php echo $name; ?></div></div>
                            <div class="panel-body post">

                            <p><?php echo $desc; ?></p>

                            </div>
                        </div>
                        <div class="margin-8"></div>
                        <div class="panel panel-info">
                            <div class="panel-heading"><div class="sidebar-header">Крафт <?php echo $name; ?></div></div>
                                <div class="panel-body post">
                                    <div class="table">
                                        <table class = "table table-bordered">
                                            <tr>
                                                <th>Ингредиенты</th>
                                                <th>Крафт</th>
                                            </tr>
                                            <?php foreach ($craftarmor as $craft): ?>
                                            <tr>
                                                <td class="craft"><p><?php echo $craft->ingcraft; ?></p></td>
                                                <td class="craft"><img src="<?php echo $craft->imgcraft; ?>" alt=""></td>
                                            </tr>
                                            <?php endforeach ?>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    
                </div>

            </div>
            
        </div>
</div>