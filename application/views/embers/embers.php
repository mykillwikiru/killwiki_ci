<!--Content LeftBlock START-->
<div class="col-lg-12">
                    <h1>Embers Rekindled</h1>
                    

                    <div class="row">
                        <div class="well clearfix">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">

                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                      Установка мода Embers Rekindled
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body modhidden">
                                    <p>
                                    <span>Для Minecraft версии 1.12.2</span>
                                    <br>
                                    <br>
                                    Версия мода при составлении Wiki - - 1.13-hotfixed2
                                    <br>
                                    <a href="https://www.curseforge.com/minecraft/mc-mods/embers-rekindled/files/2697221">Скачать мод</a>
                                    <br>
                                    Требуемый <a href= "http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html" >Forge 14.23.5.2847</a>
                                    <br>
                                    Скаченный мод поместить в папку mods вашей сборки
                                    <br>
                                    При установке в MultiMC
                                    <br>
                                    1. Редактировать сборку
                                    <br>
                                    2. Вкладка "Модификации
                                    <br>
                                    3. Тыкнуть "Добавить
                                </p>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                      Генерация руд
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя руды</th>
                                            </tr>
                                            <?php foreach($oresEmber as $ore):?>
                                            <tr>
                                                <td><img src="<?php echo $ore->imgdir;?>" alt=""></td>
                                                <td><a href="<?php echo $ore->dir;?>"><?php echo $ore->name;?></a></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Инструменты
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя инструмента</th>
                                                <th>Описание инструмента</th>
                                            </tr>
                                            <?php foreach($toolsEmber as $tool):?>
                                            <tr>
                                                <td><img src="<?php echo $tool->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $tool->dir;?>"><?php echo $tool->name;?></a></td>
                                                <td><?php echo $tool->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFour">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                      Продвинутые Инструменты
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя инструмента</th>
                                                <th>Описание инструмента</th>
                                            </tr>
                                            <?php foreach($advtoolsEmber as $tool):?>
                                            <tr>
                                                <td><img src="<?php echo $tool->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $tool->dir;?>"><?php echo $tool->name;?></a></td>
                                                <td><?php echo $tool->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFive">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                      Транспортные предметы
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя предмета</th>
                                                <th>Описание предмета</th>
                                            </tr>
                                            <?php foreach($transportEmber as $trans):?>
                                            <tr>
                                                <td><img src="<?php echo $trans->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $trans->dir;?>"><?php echo $trans->name;?></a></td>
                                                <td><?php echo $trans->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSix">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                      Энергетические ячейки Embers
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя предмета</th>
                                                <th>Описание предмета</th>
                                            </tr>
                                            <?php foreach($energycellEmber as $cell):?>
                                            <tr>
                                                <td><img src="<?php echo $cell->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $cell->dir;?>"><?php echo $cell->name;?></a></td>
                                                <td><?php echo $cell->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSeven">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                      Прочие предметы
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя предмета</th>
                                                <th>Описание предмета</th>
                                            </tr>
                                            <?php foreach($otheritemsEmber as $other):?>
                                            <tr>
                                                <td><img src="<?php echo $other->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $other->dir;?>"><?php echo $other->name;?></a></td>
                                                <td><?php echo $other->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingEight">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                      Модификаторы для оружия и брони
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя предмета</th>
                                                <th>Название зачарования</th>
                                            </tr>
                                            <?php foreach($modifiersEmber as $mod):?>
                                            <tr>
                                                <td><img src="<?php echo $mod->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $mod->dir;?>"><?php echo $mod->name;?></a></td>
                                                <td><?php echo $mod->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingNine">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                      Главные блоки Embers
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя предмета</th>
                                                <th>Описание предмета</th>
                                            </tr>
                                            <?php foreach($mainblockEmber as $main):?>
                                            <tr>
                                                <td><img src="<?php echo $main->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $main->dir;?>"><?php echo $main->name;?></a></td>
                                                <td><?php echo $main->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingTen">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                      Блоки для Алхимии Embers
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя предмета</th>
                                                <th>Описание предмета</th>
                                            </tr>
                                            <?php foreach($alchemyEmber as $alc):?>
                                            <tr>
                                                <td><img src="<?php echo $alc->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $alc->dir;?>"><?php echo $alc->name;?></a></td>
                                                <td><?php echo $alc->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingEleven">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                      Броня Embers
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя брони</th>
                                            </tr>
                                            <?php foreach($armorEmber as $armor):?>
                                            <tr>
                                                <td><img src="<?php echo $armor->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $armor->dir;?>"><?php echo $armor->name;?></a></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              



                            </div>
                            
                                
                
                        </div>
                    </div>
                    <div class="margin-8"></div>
                </div>