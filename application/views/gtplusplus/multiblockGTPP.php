
<div class="panel panel-info">
    <div class="panel-heading">
        <div class="sidebar-header">
            <a href="/gtplusplus" class="btn btn-lg btn-warning">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"> GT++ (GTplusplus)</span>
            </a>
        </div>
    </div>
        <div class="panel-body mainitemmod">
            <div class="row">
                
                <!-- block sitebaritem смещается вверх при меньших разрешениях-->
                <div class="col-lg-4 col-lg-push-8">
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header"><?php echo $name; ?></div></div>
                            <div class="panel-body itemtable">
                                <img class="img-responsive" src="<?php echo $img ?>" alt="">
                                <?php foreach ($tableinmultiblocks as $table): ?>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td class="leftinfo">Режим мультиблока:</td>
                                            <td><?php echo $table->type; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="leftinfo">Защита от дождя:</td>
                                            <td><?php echo $table->damage; ?></td>
                                        </tr>
                                    </table>
                                    <?php endforeach ?>       
                            </div>
                    </div>
                </div>
                <!-- contentitem смещается вниз при меньших разрешениях-->
                <div class="col-lg-8 col-lg-pull-4">
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Описание предмета <?php echo $name; ?></div></div>
                            <div class="panel-body post">

                            <p><?php echo $desc; ?></p>
                            

                            </div>
                        </div>
                        <div class="margin-8"></div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Крафт <?php echo $name; ?></div></div>
                            <div class="panel-body post">
                            <div class="table">                                         
                                <table class = "table table-bordered">
                                    <tr>
                                        <th>Ингредиенты</th>
                                        <th>Крафт</th>
                                    </tr>
                                <?php foreach ($defgtpp as $craft) : ?>
                                    <tr>
                                        <td class="craft"><p><?php echo $craft->ingcraft; ?></p></td>
                                        <td class="craft"><img src="<?php echo $craft->imgcraft; ?>" alt=""></td>
                                    </tr>
                                <?php endforeach ?>
                                </table>
                            </div>
                            </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Крафт в Assembler</div></div>
                            <div class="panel-body post">
                                <div class="table">                                         
                                <table class = "table table-bordered">
                                    <tr>
                                        <th>Ингредиенты</th>
                                        <th>Крафт</th>
                                    </tr>
                                <?php foreach ($assgtpp as $ass) : ?>
                                    <tr>
                                        <td class="craft"><p><?php echo $ass->ingcraft; ?></p></td>
                                        <td class="craft"><img src="<?php echo $ass->imgcraft; ?>" alt=""></td>
                                    </tr>
                                <?php endforeach ?>
                                </table>
                                </div>
                            </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Крафт в Scanner</div></div>
                            <div class="panel-body post">
                                <div class="table">                                         
                                <table class = "table table-bordered">
                                    <tr>
                                        <th>Ингредиенты</th>
                                        <th>Крафт</th>
                                    </tr>
                                <?php foreach ($scangtpp as $scanner) : ?>
                                    <tr>
                                        <td class="craft"><p><?php echo $scanner->ingcraft; ?></p></td>
                                        <td class="craft"><img src="<?php echo $scanner->imgcraft; ?>" alt=""></td>
                                    </tr>
                                <?php endforeach ?>
                                </table>
                                </div>
                            </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Крафт в Assembly Line</div></div>
                            <div class="panel-body post">
                                <div class="table">                                         
                                <table class = "table table-bordered">
                                    <tr>
                                        <th>Ингредиенты</th>
                                        <th>Крафт</th>
                                    </tr>
                                <?php foreach ($asslinegtpp as $assline) : ?>
                                    <tr>
                                        <td class="craft"><p><?php echo $assline->ingcraft; ?></p></td>
                                        <td class="craft"><img src="<?php echo $assline->imgcraft; ?>" alt=""></td>
                                    </tr>
                                <?php endforeach ?>
                                </table>
                                </div>
                            </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Интерфейс <?php echo $name; ?></div></div>
                            <div class="panel-body post">
                                <?php foreach ($interfacegtpp as $interface) : ?>
                                    <img class="img-responsive" src="<?php echo $interface->imginterface; ?>" alt="">
                                    <br>
                                    <p><?php echo $interface->nameinterface; ?></p>
                                <?php endforeach ?>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Требуется для сборки <?php echo $name; ?></div></div>
                            <div class="panel-body post">
                                <div class="table">
                                <table class = "table table-bordered">
                                    <tr>
                                        <th>Имя сборочного блока</th>
                                        <th>Количество</th>
                                    </tr>
                                <?php foreach ($multiass as $ass) : ?>
                                    <tr>
                                        <td class="leftinfocell"><?php echo $ass->nameass; ?></td>
                                        <td><?php echo $ass->amountass; ?></td>
                                    </tr>
                                <?php endforeach ?>
                                </table>
                                </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Построение <?php echo $name; ?></div></div>
                            <div class="panel-body post">
                                <?php //foreach ($tableinmultiblocks as $table) : ?>
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item"  src="<?php //echo $table->link; ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <?php //endforeach ?>
                        </div>
                    </div>
                </div>

            </div>
            
        </div>
</div>