<!--Content LeftBlock START-->
<div class="col-lg-12">
                    <h1>GT++</h1>
                    

                    <div class="row">
                        <div class="well clearfix">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">

                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                      Установка мода GT++
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body modhidden">
                                    <p>
                                    <span>Для Minecraft версии 1.7.10</span>
                                    <br>
                                    Требуемый <a href= "http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.7.10.html" >Forge 10.13.4.1614</a>
                                    <br>
                                    <br>
                                    Скаченный <a href="/assets/Mods.zip">пак</a> для корректной работы мода, распаковать и поместить в папку mods вашей сборки
                                    <br>
                                    При установке в MultiMC
                                    <br>
                                    1. Редактировать сборку
                                    <br>
                                    2. Вкладка "Модификации
                                    <br>
                                    3. Тыкнуть "Добавить
                                </p>
                                  </div>
                                </div>
                              </div>
                              
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Инструменты
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя инструмента</th>
                                                <th>Описание инструмента</th>
                                            </tr>
                                            <?php foreach($toolsGTPP as $tool):?>
                                            <tr>
                                                <td><img src="<?php echo $tool->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $tool->dir;?>"><?php echo $tool->name;?></a></td>
                                                <td><?php echo $tool->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFive">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                      Машинные компоненты
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя компонента</th>
                                                <th>Описание компонента</th>
                                            </tr>
                                            <?php foreach($machcompGTPP as $trans):?>
                                            <tr>
                                                <td><img src="<?php echo $trans->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $trans->dir;?>"><?php echo $trans->name;?></a></td>
                                                <td><?php echo $trans->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSix">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                      Одиночные машины производств
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя машины</th>
                                                <th>Описание машины</th>
                                            </tr>
                                            <?php foreach($simplemachGTPP as $sm):?>
                                            <tr>
                                                <td><img src="<?php echo $sm->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $sm->dir;?>"><?php echo $sm->name;?></a></td>
                                                <td><?php echo $sm->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSeven">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                      Утилитарные машины
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя машины</th>
                                                <th>Описание машины</th>
                                            </tr>
                                            <?php foreach($utilmachGTPP as $other):?>
                                            <tr>
                                                <td><img src="<?php echo $other->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $other->dir;?>"><?php echo $other->name;?></a></td>
                                                <td><?php echo $other->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingEight">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                      Блоки для построения мультиблоков
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя блока</th>
                                                <th>Описание блока</th>
                                            </tr>
                                            <?php foreach($utilblockformultiGTPP as $mod):?>
                                            <tr>
                                                <td><img src="<?php echo $mod->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $mod->dir;?>"><?php echo $mod->name;?></a></td>
                                                <td><?php echo $mod->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>                                        
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingNine">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                      Мультиблоки GT++
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя мультиблока</th>
                                                <th>Описание мультиблока</th>
                                            </tr>
                                            <?php foreach($multiblockGTPP as $m):?>
                                            <tr>
                                                <td><img src="<?php echo $m->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $m->dir;?>"><?php echo $m->name;?></a></td>
                                                <td><?php echo $m->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                                
                
                        </div>
                    </div>
                    <div class="margin-8"></div>
                </div>