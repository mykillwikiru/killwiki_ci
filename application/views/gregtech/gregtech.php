<!--Content LeftBlock START-->
                <div class="col-lg-12">
                    <h1>Gregtech 5</h1>
                    
                    <div class="row">
                        <div class="well clearfix">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">

                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                      Установка мода GregTech_5
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body modhidden">
                                    <p>
                                    <span>Для Minecraft версии 1.7.10</span>
                                    <br>
                                    <br>
                                    Версия мода при составлении Wiki - - 5.09.31
                                    <br>
                                    <a href="https://minecraft.curseforge.com/projects/gregtech-5-unofficial/files/2479882">Скачать мод</a>
                                    <br>
                                    Требуемый <a href= "http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.7.10.html" >Forge 10.13.4.1614</a>
                                    <br>
                                    Требуемый мод <a href="https://minecraft.curseforge.com/projects/industrial-craft/files/2353971">Industrial craft  2.2.287-experimental</a>
                                    <br>
                                    Скаченный мод поместить в папку mods вашей сборки
                                    <br>
                                    При установке в MultiMC
                                    <br>
                                    1. Редактировать сборку
                                    <br>
                                    2. Вкладка "Модификации
                                    <br>
                                    3. Тыкнуть "Добавить
                                </p>
                                  </div>
                                </div>
                              </div>

                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                      Инструменты (Tools)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body modhidden">
                                        <p>
                                            Эти инструменты могут взаимодейстовать с миром, то есть, копать руду и прочее 
                                            <br>
                                            Не участвуют в крафтах
                                            <br>
                                            При крафте играет роль материал оголовья
                                        </p>
                                        <br>
                                        <br>
                                        <div class="table">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название инструмента</th>
                                                    <th>Описание</th>
                                                </tr>
                                            <?php foreach ($tools as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                        </div>    
                                    </div>
                                </div>
                              </div>

                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Продвинутые Инструменты (Advanced Tools)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body modhidden">
                                    Эти инструменты могут взаимодейстовать с миром, то есть, тыкать по машинам и прочее
                                    <br>
                                    Участвуют в крафтах
                                    <br>
                                    При крафте играет роль материал инструмента или его главный компонент.
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название инструмента</th>
                                                    <th>Описание</th>
                                                </tr>
                                            <?php foreach ($advtools as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFour">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                      Электро-Инструменты (Electro-Tools)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                  <div class="panel-body modhidden">
                                    Данным инструментам нужна энергия
                                    <br>
                                    Они тратят и энергию и прочность в меньшей степени
                                    <br>
                                    Учитывать Tier инструмента при зарядке, если LV, то только 32 E/t, MV - 128 E/t, HV - 512 E/t.
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название инструмента</th>
                                                    <th>Описание</th>
                                                </tr>
                                            <?php foreach ($electools as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFive">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                      Мульблоки(Multiblock)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                  <div class="panel-body modhidden">
                                    Это мультиблоки, которые пригодятся для всяких вкусняшек( стали, алюминия или нержавейки и прочие материалы).
                                    <br>
                                    Мультиблоки имеют свои построения, нюансы и прочее.Тыкнув по ним, вы перейдете на подробное описание, как их строить, и прочее.
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название мультиблока</th>
                                                    <th>Описание</th>
                                                </tr>
                                            <?php foreach ($multiblocks as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSix">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                      Блоки для мультиблоков(Block for Multiblocks)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                  <div class="panel-body modhidden">
                                    Данные блоки являются составом в построении Мультиблоков Gregtech.
                                    <br>
                                    Каждый блок, зависимый от Tier может включать себя увеливение слотов предметов и увеличение емкости жидкостей.
                                    <br>
                                    А также есть обслуживаемые блоки, где некоторые периодически чинить или выпускать загрязнения при выработке машины.
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название блока</th>
                                                    <th>Описание</th>
                                                    <th>Tier</th>
                                                </tr>
                                            <?php foreach ($Blockformultiblock as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                    <td><?php echo $t->tier; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSeven">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                      Генерация руд(World Gen)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                  <div class="panel-body modhidden">
                                  Итак, генерация руд от Gregtech 5 отличается от генерации ванильного Minecraft. Здесь усложнен поиск руд, их копка, уровень копания и прочее. Данные руды могут содержать несколько субпродуктов после переработки. Типы переработок тоже разные, начиная от дробления и заканчивая химическим промыванием.
                                  <br><br>
                                  Первое отличие это два типа генерации руд, это маленькие жилы (Small Ores), которые при копке дадут вам пыльку соответствующей руды и Смешанные споты руд (Ore Mix Vein) - блок руды с дальней переработкой в будущем. Маленькие жилы, потребуются для мелких крафтов, самого необходимого, а вот большие жилы, потребуют попотеть, для их копки. А так же генерация этих жил в разных измерениях: End, Nether, Overworld, планеты(GT New Horizons), Twilight Forest (GTNH) и так далее.
                                  <br><br>
                                  Второе отличие, это их шанс генерации в мире(к биомам не привязаны, слава богу!), у мелких жил шанс более высок и на разных высотах. А вот крупные жилы, они зависят, от высоты (не проканает ванильная копка); от типа местности (ни в земле, ни в гравии и т д), потому что алгоритм генерации данного типа заменят только блок камня, на соответствующую руды жилы. Но у крупных жил еще есть своя высота, слои (как в торте) руд(первичная, вторичная, промежуточная и вкрапленная), своему размеру, могут быть в размере 1 чанк, а могут размахнутся до 3-4 чанков(зависит от жилы).
                                  <br><br>
                                  И третье - переработка. Для мелких жилок она не потребуется, но для крупных жил, еще как, для удвоения руб, получения многих типов побочек при дроблении, водном и химическом промывании, даже в центрифуге и электролизере. Где потом полученыые пыльки прожарить в печке, или Доменной печи(если требуется), или в получении чего-то из неорганической химии или получении газов, начиная с водорода и кислорода до аргона.
                                  <br><br>
                                  Ниже указаны ссылки на таблицу генерации руд в ванильном Gregtech, а вот ссылка генерация руд для сборки GTNH <a href="https://docs.google.com/spreadsheets/d/1Rsz0rH9tIVJxr18b1Z6-QxOSaEKssxF7u2naQTq2Mqg/edit#gid=1460741178">генерация руд для сборки GTNH</a>. И переходе на ссылки, вы узнаете как добыть руды и их выосты и шансы генерации.
                                  <br><br>
                                  <table class="table table-bordered oregen">
                                    <tr>
                                        <th class="greycell"><a href="/gregtech/smalloresgt">Small Ores</a></th>
                                        <th class="greycell"><a href="/gregtech/oremixveingt">Ore Mix Vein</a></th>
                                    </tr>
                                  </table>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingEight">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                      Переработка руд(Ore Processing)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                  <div class="panel-body modhidden">
                                    <img class="img-responsive" src="/assets/img/gregtech/Oreprocessing.png" alt="">
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingNine">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                      Химическая переработка(Chemical Processing)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                  <div class="panel-body modhidden">
                                  Чтож,тут указаны первые этапы переработок со всех типов нефти на составляющие газы и жидкости с производством некоторых химических элементов.
                                    <br><br>
                                    <div class="well">
                                    <span>0 Этап - Добыча</span>
                                    <br><br>
                                    Первым делом вам нужно добыть любой из типов нефти (Oil, Raw Oil, Heavy Oil, Light Oil). Можно добыть с помощью <a href="/gregtech/electromachinesgt/pump">Pump</a> или <a href="/gregtech/multiblocksgt/oil-drilling-rig">Oil Drilling Rig</a>.
                                    </div>
                                    <br><br>
                                    <div class="well">
                                    <span>1 Этап - Дистилляция</span>
                                    <br><br>
                                    Добытые вами нефть, помещаем в <a href="/gregtech/electromachinesgt/distillery">Distillery</a> или <a href="/gregtech/multiblocksgt/distillation-tower">Distillation Tower</a> (по уровню развития) и получаем сульфуцированные жидкости и газы.
                                    </div>
                                    <br><br>
                                    <div class="well">
                                    <span>2 Этап - Очищение от сульфуцирования</span>
                                    <br><br>
                                    Поместив сульфуцированные жидкости и газы в <a href="/gregtech/electromachinesgt/chemical-reactor">Chemical reactor</a> с Hydrogen, мы можем получать первые газы после нефтепереработки.
                                    </div>
                                    <br><br>
                                    <div class="well">
                                    <span>3 Этап - Нагревание водородом и паром</span>
                                    <br><br>
                                    Это промежуточный этап, здесь вы можете получить первые хим элементы или дальнейшая обработка для газового топлива. Для продолжения этапа нужно любой из газов для дальней обработки (Naphtha, Light Fuel, Heavy Fuel и Refinery Gas) и поместить в <a href="/gregtech/electromachinesgt/chemical-reactor">Chemical reactor</a> или в <a href="/gregtech/multiblocksgt/oil-cracking-unit">Oil Craking Unit</a> с капсулами Hydrogen или Steam.
                                    </div>
                                    <br><br>
                                    <div class="well">
                                    <span>4 Этап - Дистилляция нагретых нефтепродуктов</span>
                                    <br><br>
                                    Далее полученные жидкости помещаем в <a href="/gregtech/electromachinesgt/distillery">Distillery</a> или <a href="/gregtech/multiblocksgt/distillation-tower">Distillation Tower</a> (по уровню развития) для получения первых газов. Такие как: Methane, Butene, Butane, Butadiene, Propane, Propene, Ethane, Benzene, Ethylene, Toluene,  Helium, Hydrogen, Light Fuel, Heavy Fuel и Naphtha.
                                    <br>
                                    <a href="/assets/img/gregtech/chem.png">Смотри или скачай</a> для себя схему переработки с 0-4 Этап.
                                    </div>
                                    <br><br>
                                    <div class="well">
                                    <span>5 Этап - Создание некоторых химических соединений</span>
                                    <br><br>
                                    Далее некоторые полученные жидкости (Helium, Toluene, Ethylene, Propene, Butadiene, Benzene, Methane) помещаем в в <a href="/gregtech/electromachinesgt/chemical-reactor">Chemical reactor</a>, или <a href="/gregtech/multiblocksgt/large-chemical-reactor">Large Chemical Reactor</a>, или <a href="">Centrifuge</a> или <a href="">Fusion Reactor</a>, ради получения многих химических соединений. А  остальные - в следующий этап.
                                    </div>
                                    <br><br>
                                    <div class="well">
                                    <span>6 Этап - Нагревание водородом и паром</span>
                                    <br><br>
                                    Это промежуточный этап, здесь вы можете получить первые хим элементы или дальнейшая обработка для газового топлива. Для продолжения этапа нужно любой из газов для дальней обработки (Butane, Butene, Butadiene, Propane, Propene, Ethane и Ethylene) и поместить в <a href="/gregtech/electromachinesgt/chemical-reactor">Chemical reactor</a> или в <a href="/gregtech/multiblocksgt/oil-cracking-unit">Oil Craking Unit</a> с капсулами Hydrogen или Steam. То есть данный этап уходит в цикл 5.
                                    </div>
                                    <br><br>
                                    <div class="well">
                                    <span>7 Этап - Дистилляция нагретых нефтепродуктов</span>
                                    <br><br>
                                    Далее полученные жидкости помещаем в <a href="/gregtech/electromachinesgt/distillery">Distillery</a> или <a href="/gregtech/multiblocksgt/distillation-tower">Distillation Tower</a> (по уровню развития) для получения первых газов для повторного цикла этапа 5.
                                    <br>
                                    <a href="/assets/img/gregtech/chem2.png">Смотри или скачай</a> для себя схему переработки с 5-7 Этап.
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingTen">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                      Бронзовый век(Bronze Age)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                  <div class="panel-body modhidden">
                                    Итак здесь начало первого века по развитию в Gregtech, это у вас будут первые машинки, которые подтолкнут вас к автоматиции рутины( такие как переработка руд, начальных сплавов, облегчение получния компонентов и прочее).
                                    <br>
                                    Первым делом вам нужно найти Медную и Оловяную руду, для получения первого сплава, под названием Bronze ingot. Потому что его потребуеться немного и не мало для создания машин данного Tier.
                                    Также вам нужно сделать уже продвинутые инструменты, потому что начальная эра требует пластины и другие компонеты крафтов (да у каждого блока свой рецепт)
                                    <br>
                                    Бойлеры вырабатывают пар, а другие обрабатывают металлы и прочие руды с блоками.
                                    А также вам нужно сделать первый мультиблок Bricked Blust Furnace для получения слитков стали.
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название блока</th>
                                                    <th>Описание</th>                                               
                                                </tr>
                                            <?php foreach ($bronzeage as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingEleven">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                      Век высокого давления(Hige Pressure Age)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                                  <div class="panel-body modhidden">
                                    Следующий Tier по развитию Gregtech, это Hige pressure машины, но тут они быстрее выполняют операции (не значительно конечно) и они дают скачок в другую эру, когда вы развили данный Tier от прошлого можно потихоньку отказываться.
                                    Далее вам нужно найти Угольно и Железо-содержащие руды, для того чтобы получить первый Steel ingot. Он потребуется уже для машин данной Эры, и когда у вас много бронзы можно уже сделать Bronze Blast Furnace.
                                    И да нужно уже запасатся Сталью, потому что данный Век быстроиграющий, ибо на подходе LV-Tier(а там электричество и множество разных плюшек).
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название блока</th>
                                                    <th>Описание</th>                                               
                                                </tr>
                                            <?php foreach ($hpage as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingTwelve">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                                      Батарейки (Battery)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
                                  <div class="panel-body modhidden">
                                    Здесь указаны все типы батареек, которые являются хранилищем энергии
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название батарейки</th>
                                                    <th>Tier</th>                                               
                                                </tr>
                                            <?php foreach ($battery as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->tier; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingThirteen">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                                      Машинные компоненты (Machine components)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                                  <div class="panel-body modhidden">
                                    Здесь указаны помпы и т д
                                    Данные компоненты можно прикреплять к машинам, и настраивать screwdriver, и также могут использоваться в крафтах для машин.
                                    А вот Data stick и Data Orb могут хранить информацию крафта при сканировании какого либо предмета.
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название компонента</th>
                                                    <th>Описание</th>
                                                    <th>Tier</th>                                               
                                                </tr>
                                            <?php foreach ($machcomp as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                    <td><?php echo $t->tier; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFourteen">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
                                      Прочие предметы (Others items)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFourteen">
                                  <div class="panel-body modhidden">
                                    Данные предметы могут переносить жидкость, предметы, энергию и использоваться в крафтах.
                                    Даже использоваться в крафтах и быть в составе мультиблоков.
                                    При крафте играет роль материал, для быстроты переноса.
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название предмета</th>
                                                    <th>Описание</th>                                        
                                                </tr>
                                            <?php foreach ($otheritems as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFifteen">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
                                      Микросхемы (Circuits)[All Tiers]
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen">
                                  <div class="panel-body modhidden">
                                    Здесь указаны микросхемы всех Tier по возрастанию.
                                    Все микросхемы взаимозаменяемы в крафтах машин, одни делаются легче, другие повеселее.
                                    А вот Programmed circuit может быть в инвентарях машин (и одноблоков и мультиблоков)
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название микросхемы</th>
                                                    <th>Tier</th>                                        
                                                </tr>
                                            <?php foreach ($circuits as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->tier; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSixteen">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
                                      Электрические машины(Electro machines)[All Tiers]
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSixteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSixteen">
                                  <div class="panel-body modhidden">
                                    Здесь указаны машины, некоторые машины имеют одну и ту же операцию, но названия под каким либо Tier`ом меняються.(так что тут они вписаны)
                                    <br>
                                    Под понятием ALL TIERS обозначаеться, что все машины входят во все Tier:
                                    <br>
                                    <span>(LV->MV->HV->EV->IV->LuV->ZPM->UV)</span>
                                    <br>
                                    Эти машины могут выработать энегрию, что то могут и делать с материалами и рудами и жидкостями.
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название машины</th>
                                                    <th>Описание машины</th>
                                                    <th>Tier</th>                                        
                                                </tr>
                                            <?php foreach ($electromachines as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                    <td><?php echo $t->tier; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSeventeen">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen" aria-expanded="false" aria-controls="collapseSeventeen">
                                      Утилитарные машины(Utilit - machines)[All Tiers]
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSeventeen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeventeen">
                                  <div class="panel-body modhidden">
                                    Тут указаны блоки, которые упростят вам жизнь при сортировке или отделении предметов по OreDict , даже настроить на определенное количество предметов/жидкости
                                    Также помогут вам при проложении Элетросети. там тоже есть сови ньюансы, любая ошибка приведет к БА-БАХ.
                                    А также в них можно хранить и жидкости и предметы.
                                    <br>
                                    <br>
                                    <div class="table">
                                        <table class="table table-bordered">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Название машины</th>
                                                    <th>Описание машины</th>
                                                    <th>Tier</th>                                        
                                                </tr>
                                            <?php foreach ($utilmachines as $t): ?>
                                                <tr>
                                                    <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                    <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                                    <td><?php echo $t->descript; ?></td>
                                                    <td><?php echo $t->tier; ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingEighteen">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEighteen" aria-expanded="false" aria-controls="collapseEighteen">
                                      Броня(Armor)
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseEighteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEighteen">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Броня</th>
                                            </tr>
                                        <?php foreach ($armor as $t): ?>
                                            <tr>
                                                <td><img src="<?php echo $t->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $t->dir; ?>"><?php echo $t->name; ?></a></td>
                                            </tr>
                                        <?php endforeach ?>
                                                                
                                            </table>
                                    </div>
                                  </div>
                                </div>
                            </div>



                            </div>
                            
                                
                
                        </div>
                    </div>
                    <div class="margin-8"></div>
                </div>