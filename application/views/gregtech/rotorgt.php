<div class="panel panel-info">
<div class="panel-heading">
    <div class="sidebar-header">
        <a href="/gregtech/otheritemsgt/turbine" class="btn btn-lg btn-warning">
            <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"> Turbine</span>
        </a>
    </div>
</div>
    <div class="panel-body mainitemmod">
        <div class="row">
            
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
			<div class="panel panel-info">
				<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
					Таблица Оптимальных потоков для Роторов турбин
					<br>
					</a>
				</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body modhidden">
						<div class="table">
							<table class="table table-bordered table_sort">
								<thead>
								<tr>
									<th colspan = "2">Material</th>
									<th colspan = "5">Small Turbine Rotor</th>
									<th colspan = "5">Turbine Rotor</th>
									<th colspan = "5">Large Turbine Rotor</th>
									<th colspan = "5">Huge Turbine Rotor</th>
								</tr>
								</thead>
								<thead>
								<tr>
									<th>Name</th>
									<th>Tier</th>

									<th>Прочность</th>
									<th>Оптимальный поток пара (L/s)</th>
									<th>Оптимальный поток Газа (EU/t)</th>
									<th>Оптимальный поток плазмы (EU/t)</th>
									<th>Эффективность %</th>

									<th>Прочность</th>
									<th>Оптимальный поток пара (L/s)</th>
									<th>Оптимальный поток Газа (EU/t)</th>
									<th>Оптимальный поток плазмы (EU/t)</th>
									<th>Эффективность %</th>

									<th>Прочность</th>
									<th>Оптимальный поток пара (L/s)</th>
									<th>Оптимальный поток Газа (EU/t)</th>
									<th>Оптимальный поток плазмы (EU/s)</th>
									<th>Эффективность %</th>

									<th>Прочность</th>
									<th>Оптимальный поток пара (L/s)</th>
									<th>Оптимальный поток Газа (EU/t)</th>
									<th>Оптимальный поток плазмы (EU/s)</th>
									<th>Эффективность %</th>

								</tr>
								</thead>
								<tbody>
								<?php foreach ($Rotor as $r): ?>
								<tr>
									<td class="leftinfocell"><?php echo $r->material; ?></td>
									<td><?php echo $r->tier; ?></td>

									<td><?php echo $r->smalldurability ?></td>
									<td><?php echo $r->smallsteamflow ?></td>
									<td><?php echo $r->smallgasflow ?></td>
									<td><?php echo $r->smallplasmaflow ?></td>
									<td><?php echo $r->smallefficiency ?></td>

									<td><?php echo $r->normaldurability ?></td>
									<td><?php echo $r->normalsteamflow ?></td>
									<td><?php echo $r->normalgasflow ?></td>
									<td><?php echo $r->normalplasmaflow ?></td>
									<td><?php echo $r->normalefficiency ?></td>

									<td><?php echo $r->largedurability ?></td>
									<td><?php echo $r->largesteamflow ?></td>
									<td><?php echo $r->largegasflow ?></td>
									<td><?php echo $r->largeplasmaflow ?></td>
									<td><?php echo $r->largeefficiency ?></td>

									<td><?php echo $r->hugedurability ?></td>
									<td><?php echo $r->hugesteamflow ?></td>
									<td><?php echo $r->hugegasflow ?></td>
									<td><?php echo $r->hugeplasmaflow ?></td>
									<td><?php echo $r->hugeefficiency ?></td>
								</tr>
								<?php endforeach?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
		</div>
		<div class="panel panel-info">
                    <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Калькулятор турбин
                        </a>
                    </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body calculate">
                            <div class ="calc col-lg-12">
                                    <div class="panel panel-info">
                                        <div class="panel-heading"><div class="sidebar-header">Тип ротора и топлива</div></div>
                                            <div class="panel-body post">
                                                <div class="well col-lg-12">
                                                    <div class="well col-lg-9">
                                                        <select id="calc">
															<option value="" selected="selected">Выбери матриал ротора...</option>
															<?php foreach ($NameRotor as $n): ?>
															<option value="
															<?php echo $n->smallsteamflow ?>,
															<?php echo $n->normalsteamflow ?>,
															<?php echo $n->largesteamflow ?>,
															<?php echo $n->hugesteamflow ?>,
															<?php echo $n->smallefficiency ?>,
															<?php echo $n->normalefficiency ?>,
															<?php echo $n->largeefficiency ?>,
															<?php echo $n->hugeefficiency ?>"><?php echo $n->material;?></option>
															<?php endforeach;?>
                                                        </select>
                                                        <br>
                                                        <select id="steam">
                                                                <option value="" selected="selected">Выбери паровое топливо...</option>
                                                                <option value="0.5">Steam</option>
                                                                <option value="1">HP Steam</option>
                                                        </select>
                                                        <br>
                                                        <select id="gas">
																<option value="" selected="selected">Выбери газовое топливо...</option>
																<?php foreach ($Gas as $gas): ?>
																<option value="<?php echo $gas->value;?>"><?php echo $gas->name;?></option>
																<?php endforeach;?>
                                                        </select>
                                                        <br>
                                                        <select id="plasma">
															<option value="" selected="selected">Выбери плазменное топливо...</option>
															<?php foreach ($Plasma as $plasma): ?>
															<option value="<?php echo $plasma->value;?>"><?php echo $plasma->name;?></option>
															<?php endforeach;?>
                                                        </select>
                                                    </div>
                                                    <div class="well col-lg-3">
                                                        <button id="click" type="button" class="btn btn-success">Посчитать</button>
                                                    </div>
                                                </div> 
                                            </div>
                                    </div>
                                <div class="panel panel-info">
                                    <div class="panel-heading"><div class="sidebar-header">Small Turbine</div></div>
                                        <div class="panel-body post">
											<div class="col-lg-12"><h1>Steam Turbine</h1></div>
                                            <div class="well col-lg-6">
                                                <p>Оптимальный поток пара (L/s):<span class="label label-default" id="steamread1"></span></p>
                                            </div>
                                            <div class="well col-lg-6">
                                                <p>Оптимальный поток пара (L/t):<span class="label label-default" id="flow1"></span></p>
                                            </div>
                                            <div class="well col-lg-6">
                                                <p>Выход энергии при паровом топливе(EU/t):<span class="label label-default" id="amounteng1"></span></p>
                                            </div>
                                            <div class="well col-lg-6">
                                                <p>Требуемый Dynamo Hatch:<span class="label label-default" id="tier1"></span></p>
                                            </div>
											<div class="margin-8"></div>
											<div class="col-lg-12"><h1>Gas Turbine</h1></div>
                                            <div class="well col-lg-6">
                                                <p>Оптимальный поток газа(L/s):<span class="label label-default" id="gasread1"></span></p>
                                            </div>
                                            <div class="well col-lg-6">
                                                <p>Оптимальный поток газа(L/t):<span class="label label-default" id="gasflow1"></span></p>
                                            </div>
                                            <div class="well col-lg-6">
                                                <p>Выход энергии при газовом топливе(EU/t):<span class="label label-default" id="gasamount1"></span></p>
                                            </div>
                                            <div class="well col-lg-6">
                                                <p>Требуемый Dynamo Hatch:<span class="label label-default" id="gastier1"></span></p>
											</div>
											<div class="margin-8"></div>
											<div class="col-lg-12"><h1>Plasma Turbine</h1></div>
                                            <div class="well col-lg-6">
                                                <p>Оптимальный поток плазмы(L/s):<span class="label label-default" id="plasmaread1"></span></p>
                                            </div>
                                            <div class="well col-lg-6">
                                                <p>Оптимальный поток плазмы(L/t):<span class="label label-default" id="plasmaflow1"></span></p>
                                            </div>
                                            <div class="well col-lg-6">
                                                <p>Выход энергии при плазменном топливе(EU/t):<span class="label label-default" id="plasmaamount1"></span></p>
                                            </div>
                                            <div class="well col-lg-6">
                                                <p>Требуемый Dynamo Hatch:<span class="label label-default" id="plasmatier1"></span></p>
                                            </div>
                                        </div>
                                </div>
                                <div class="panel panel-info">
                                        <div class="panel-heading"><div class="sidebar-header">Turbine</div></div>
                                            <div class="panel-body post">
													<div class="col-lg-12"><h1>Steam Turbine</h1></div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток пара (L/s):<span class="label label-default" id="steamread2"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток пара (L/t):<span class="label label-default" id="flow2"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Выход энергии при паровом топливе(EU/t):<span class="label label-default" id="amounteng2"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Требуемый Dynamo Hatch:<span class="label label-default" id="tier2"></span></p>
                                                    </div>
                                                    <br>
                                                    <div class="col-lg-12"><h1>Gas Turbine</h1></div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток газа(L/s):<span class="label label-default" id="gasread2"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток газа(L/t):<span class="label label-default" id="gasflow2"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Выход энергии при газовом топливе(EU/t):<span class="label label-default" id="gasamount2"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                            <p>Требуемый Dynamo Hatch:<span class="label label-default" id="gastier2"></span></p>
                                                    </div>
                                                    <br>
                                                    <div class="col-lg-12"><h1>Plasma Turbine</h1></div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток плазмы(L/s):<span class="label label-default" id="plasmaread2"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток плазмы(L/t):<span class="label label-default" id="plasmaflow2"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Выход энергии при плазменном топливе(EU/t):<span class="label label-default" id="plasmaamount2"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                            <p>Требуемый Dynamo Hatch:<span class="label label-default" id="plasmatier2"></span></p>
                                                    </div>
                                            </div>
                                </div>
                                <div class="panel panel-info">
                                        <div class="panel-heading"><div class="sidebar-header">Large Turbine</div></div>
                                            <div class="panel-body post">
											<div class="col-lg-12"><h1>Steam Turbine</h1></div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток пара (L/s):<span class="label label-default" id="steamread3"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток пара (L/t):<span class="label label-default" id="flow3"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Выход энергии при паровом топливе(EU/t):<span class="label label-default" id="amounteng3"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Требуемый Dynamo Hatch:<span class="label label-default" id="tier3"></span></p>
                                                    </div>
                                                    <br>
                                                    <div class="col-lg-12"><h1>Gas Turbine</h1></div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток газа(L/s):<span class="label label-default" id="gasread3"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток газа(L/t):<span class="label label-default" id="gasflow3"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Выход энергии при газовом топливе(EU/t):<span class="label label-default" id="gasamount3"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                            <p>Требуемый Dynamo Hatch:<span class="label label-default" id="gastier3"></span></p>
                                                        </div>
                                                    <br>
                                                    <div class="col-lg-12"><h1>Plasma Turbine</h1></div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток плазмы(L/s):<span class="label label-default" id="plasmaread3"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток плазмы(L/t):<span class="label label-default" id="plasmaflow3"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Выход энергии при плазменном топливе(EU/t):<span class="label label-default" id="plasmaamount3"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                            <p>Требуемый Dynamo Hatch:<span class="label label-default" id="plasmatier3"></span></p>
                                                    </div>
                                            </div>
                                </div>
                                <div class="panel panel-info">
                                        <div class="panel-heading"><div class="sidebar-header">Huge Turbine</div></div>
                                            <div class="panel-body post">
											<div class="col-lg-12"><h1>Steam Turbine</h1></div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток пара (L/s):<span class="label label-default" id="steamread4"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток пара (L/t):<span class="label label-default" id="flow4"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Выход энергии при паровом топливе(EU/t):<span class="label label-default" id="amounteng4"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Требуемый Dynamo Hatch:<span class="label label-default" id="tier4"></span></p>
                                                    </div>
                                                    <br>
                                                    <div class="col-lg-12"><h1>Gas Turbine</h1></div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток газа(L/s):<span class="label label-default" id="gasread4"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток газа(L/t):<span class="label label-default" id="gasflow4"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Выход энергии при газовом топливе(EU/t):<span class="label label-default" id="gasamount4"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                            <p>Требуемый Dynamo Hatch:<span class="label label-default" id="gastier4"></span></p>
                                                        </div>
                                                    <br>
                                                    <div class="col-lg-12"><h1>Plasma Turbine</h1></div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток плазмы(L/s):<span class="label label-default" id="plasmaread4"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Оптимальный поток плазмы(L/t):<span class="label label-default" id="plasmaflow4"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                        <p>Выход энергии при плазменном топливе(EU/t):<span class="label label-default" id="plasmaamount4"></span></p>
                                                    </div>
                                                    <div class="well col-lg-6">
                                                            <p>Требуемый Dynamo Hatch:<span class="label label-default" id="plasmatier4"></span></p>
                                                    </div>
                                            </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
				
		</div>

        </div>
        
    </div>
</div>
