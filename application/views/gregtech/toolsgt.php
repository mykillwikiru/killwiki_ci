<div class="panel panel-info">
    <div class="panel-heading">
        <div class="sidebar-header">
            <a href="/gregtech" class="btn btn-lg btn-warning">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"> Gregtech 5</span>
            </a>
        </div>
    </div>
        <div class="panel-body mainitemmod">
            <div class="row">
                
                <!-- block sitebaritem смещается вверх при меньших разрешениях-->
                <div class="col-lg-4 col-lg-push-8">
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header"><?php echo $name; ?></div></div>
                            <div class="panel-body itemtable">
                                <img class="img-responsive" src="<?php echo $img ?>">
                                <?php foreach ($tableintools as $tools): ?>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td class="leftinfo">Тип:</td>
                                            <td><?php echo $tools->type; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="leftinfo">Прочность:</td>
                                            <td><?php echo $tools->durability; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="leftinfo">Уровень копания:</td>
                                            <td><?php echo $tools->mininglevel; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="leftinfo">Скорость копания:</td>
                                            <td><?php echo $tools->speedlevel; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="leftinfo">Зачарование:</td>
                                            <td><?php echo $tools->enchantment; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="leftinfo">Апдейт по Tier</td>
                                            <td><?php echo $tools->updatetool; ?></td>
                                        </tr>
                                    </table>
                                    <?php endforeach ?>     
                            </div>
                    </div>
                </div>
                <!-- contentitem смещается вниз при меньших разрешениях-->
                <div class="col-lg-8 col-lg-pull-4">
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Описание предмета <?php echo $name; ?></div></div>
                            <div class="panel-body post">

                            <p><?php echo $desc; ?></p>

                            </div>
                        </div>
                        <div class="margin-8"></div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Крафт <?php echo $name; ?></div></div>
                            <div class="panel-body post">

                            <p>Flint <?php echo $name; ?></p>
                            <div class="table">
                                <table class = "table table-bordered">
                                    <tr>
                                        <th>Ингредиенты</th>
                                        <th>Крафт</th>
                                    </tr>
                                    <?php foreach ($crafttools as $craft): ?>
                                    <tr>
                                        <td class="craft"><p><?php echo $craft->ingtabledefcraft ?></p></td>
                                        <td class="craft"><img src="<?php echo $craft->imgtabledefcraft ?>" alt=""></td>
                                    </tr>
                                    <?php endforeach ?>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Другие крафты <?php echo $name; ?></div></div>
                            <div class="panel-body post">

                            <p>Крафт</p>
                            <div class="table">
                            <table class = "table table-bordered">
                                <tr>
                                    <th>Ингредиенты</th>
                                    <th>Крафт</th>
                                </tr>
                                <?php foreach ($crafttools as $craft): ?>
                                <tr>
                                    <td class="craft"><p><?php echo $craft->ingothercraft ?></p></td>
                                    <td class="craft"><img src="<?php echo $craft->imgothercraft ?>" alt=""></td>
                                </tr>
                                <?php endforeach ?>
                            </table>
                            </div>
                            <br>
                            <p>Крафт в Extruder</p>
                            <div class="table">
                                <table class = "table table-bordered">
                                    <tr>
                                        <th>Ингредиенты</th>
                                        <th>Крафт</th>
                                    </tr>
                                    <?php foreach ($crafttools as $craft): ?>
                                    <tr>
                                        <td class="craft"><p><?php echo $craft->ingextcraft ?></p></td>
                                        <td class="craftextruder"><img src="<?php echo $craft->imgextcraft ?>" alt=""></td>
                                    </tr>
                                    <?php endforeach ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Таблица материалов <?php echo $name; ?></div></div>
                            <div class="panel-body post">
                                <p>В этой таблице указаны все материалы, которые влияют на разные характеристики инструмента. И сортировка работает, просто нажми по любому заголовку таблицы</p>
                                <div class="table">
                                    <table class = "table table-bordered table_sort">
                                        <thead>
                                            <tr>
                                                <th>Материал(Баф)</th>
                                                <th>Прочность</th>
                                                <th>Уровень копания</th>
                                                <th>Скорость копания</th>
                                                <th>Урон</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                    <?php foreach ($Material as $pickMat) : ?>
                                            <tr>
                                                <td class="leftinfocell"><?php echo $pickMat->material; ?></td>
                                                <td><?php echo $pickMat->durability; ?></td>
                                                <td><?php echo $pickMat->mininglevel; ?></td>
                                                <td><?php echo $pickMat->miningspeed; ?></td>
                                                <td><?php echo $pickMat->damage; ?></td>
                                            </tr>
                                    <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>

                        </div>
                    </div>
                </div>

            </div>
            
        </div>
</div>