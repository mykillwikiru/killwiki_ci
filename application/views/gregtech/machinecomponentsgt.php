<div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="sidebar-header">
                            <a href="/gregtech" class="btn btn-lg btn-warning">
                                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"> Gregtech 5</span>
                            </a>
                        </div>
                    </div>
                        <div class="panel-body mainitemmod">
                            <div class="row">
                                
                                <!-- block sitebaritem смещается вверх при меньших разрешениях-->
                                <div class="col-lg-4 col-lg-push-8">
                                    <div class="panel panel-info">
                                        <div class="panel-heading"><div class="sidebar-header"><?php echo $name; ?></div></div>
                                            <div class="panel-body itemtable">
                                                <img class="img-responsive" src="<?php echo $img ?>" alt="brickedBF">
                                                <?php foreach ($tableinmachcompgt as $table): ?>
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <td class="leftinfo">Тип:</td>
                                                            <td><?php echo $table->type; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leftinfo">Тип энергии:</td>
                                                            <td><?php echo $table->voltage; ?></td>
                                                        </tr>
                                                    </table>  
                                                    <?php endforeach ?>    
                                            </div>
                                    </div>
                                </div>
                                <!-- contentitem смещается вниз при меньших разрешениях-->
                                <div class="col-lg-8 col-lg-pull-4">
                                    <div class="panel panel-info">
                                        <div class="panel-heading"><div class="sidebar-header">Описание предмета <?php echo $name; ?></div></div>
                                            <div class="panel-body post">

                                            <p><?php echo $desc; ?></p>

                                            </div>
                                        </div>
                                        <div class="margin-8"></div>
                                    <div class="panel panel-info">
                                        <div class="panel-heading"><div class="sidebar-header">Крафт <?php echo $name; ?></div></div>
                                            <div class="panel-body post">      
                                            <div class="table">                                   
                                                <table class = "table table-bordered">
                                                    <tr>
                                                        <th>Ингредиенты</th>
                                                        <th>Крафт</th>
                                                        <th>Tier</th>
                                                    </tr>
                                                <?php foreach ($machcompcraft as $craft) : ?>
													<tr>
														<td class="craft"><p><?php echo $craft->ingcraft; ?></p></td>
														<td class="craft"><img src="<?php echo $craft->imgcraft; ?>" alt=""></td>
                                                        <td class="craft"><p><?php echo $craft->tier; ?></p></td>
													</tr>
												<?php endforeach ?>
                                                </table>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="panel panel-info">
                                        <div class="panel-heading"><div class="sidebar-header">Крафт в Assembly Line</div></div>
                                            <div class="panel-body post">
                                            <div class="table">                                         
                                                <table class = "table table-bordered">
                                                    <tr>
                                                        <th>Ингредиенты</th>
                                                        <th>Крафт</th>
                                                        <th>Tier</th>
                                                    </tr>
                                                <?php foreach ($assline as $ass) : ?>
													<tr>
														<td class="craft"><p><?php echo $ass->assing; ?></p></td>
														<td class="craft"><img src="<?php echo $ass->assimg; ?>" alt=""></td>
                                                        <td class="craft"><p><?php echo $ass->tier?></p></td>
													</tr>
												<?php endforeach ?>
                                                </table>
                                                </div>
                                            </div>
                                    </div>


                                </div>

                            </div>
                            
                        </div>
                </div>