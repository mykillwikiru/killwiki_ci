<div class="panel panel-info">
    <div class="panel-heading">
        <div class="sidebar-header">
            <a href="/gregtech" class="btn btn-lg btn-warning">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"> Gregtech 5</span>
            </a>
        </div>
    </div>
        <div class="panel-body mainitemmod">
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Описание про Small Ores</div></div>
                            <div class="panel-body post">

                                <p>Данный тип руд генерируется в разных высотах и в рандомном месте по одному блоку. Нет зависимости от координат чанка.</p>
                                <br>
                                <img class="img-responsive" src="/assets/img/gregtech/smalloregen.png">
                                <br>
                                <br>
                                <p>Чтоб наглядно представлять вид генерации данных руд, то при 1 - это генерация Small ores</p>
                                <br>
                                <img class="img-responsive" src="/assets/img/gregtech/allores.png">
                                <br>
                                <p>Вскапывается <a href="/gregtech/toolsgt/pickaxe">Pickaxe</a> или <a href="/gregtech/electoolsgt/drill">Drill</a> ,
                                 в качестве дропа выпадает Crushed Ore, Impure Pile of Dust, Dust материала руды и каменной пыли. 
                                 Silk Touch не работает с данными рудами. Ниже указана таблица генерация руд.
                                </p>

                            </div>
                        </div>
                        <div class="margin-8"></div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Таблица Small Ores Gen</div></div>
                            <div class="panel-body post">
                                <p>В этой таблице указаны все руды и их местонахождения</p>
                                <div class="table">
                                    <table class = "table table-bordered table_sort">
                                        <thead>
                                            <tr>
                                                <th>Материал</th>
                                                <th>Минимальная высота</th>
                                                <th>Максимальная высота</th>
                                                <th>Редкость жилы</th>
                                                <th>Overworld</th>
                                                <th>End</th>
                                                <th>Nether</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($SmallOresGen as $SmallGen):?>
                                            <tr>
                                                <td class="leftinfocell"><?php echo $SmallGen->material?></td>
                                                <td><?php echo $SmallGen->minheight?></td>
                                                <td><?php echo $SmallGen->maxheight?></td>
                                                <td><?php echo $SmallGen->rarity?></td>
                                                <td><?php echo $SmallGen->overworld?></td>
                                                <td><?php echo $SmallGen->nether?></td>
                                                <td><?php echo $SmallGen->end?></td>
                                            </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>

                        </div>
                    </div>
                </div>

            </div>
            
        </div>
</div>