<div class="panel panel-info">
    <div class="panel-heading">
        <div class="sidebar-header">
            <a href="/gregtech" class="btn btn-lg btn-warning">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"> Gregtech 5</span>
            </a>
        </div>
    </div>
        <div class="panel-body mainitemmod">
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Описание про Ore Mix Vein</div></div>
                            <div class="panel-body post">

                            <p>Данный тип руд генерируется в разных высотах, размере и толщине. Уже зависит от координат чанка.</p>
                            <br>
                            <img class="img-responsive" src="/assets/img/gregtech/viewmix.png">
                            <br>
                            <br>
                            <p>Чтоб наглядно представлять вид генерации данных руд, то при 2 - это генерация Ore Mix Vein</p>
                            <br>
                            <img class="img-responsive" src="/assets/img/gregtech/allores.png">
                            <br>
                            <p>Вскапывается <a href="/gregtech/toolsgt/pickaxe">Pickaxe</a> или <a href="/gregtech/electoolsgt/drill">Drill</a> , 
                            <a href="/gregtech/electromachinesgt/miner">Miner</a>
                            в качестве дропа выпадает руда в чистом виде. Также добывается с помощью 
                            <a href="/gregtech/multiblocksgt/ore-drilling-plant">Ore Drilling Plant</a>, но в качестве дропа уже руда в дробленном виде.
                            Ниже указана таблица генерация руд.
                            </p>
                            <br>
                            <h1>Как добывать руду:</h1>
                            <p>1. Нулевой чанк = координата вашего спавна в мире. Как только вы заспавнились в мире, вы находитесь в нулевом чанке. И это начало отсчета для георазведки.</p>
                            <img class="img-responsive" src="/assets/img/gregtech/nullchunk.png">
                            <br>
                            <p>2. Промежуточные центральные чанки руд, помечены зеленым. Если рядом с нулевым чанком по диагонали, могут содержать центры предполагаемых жил. 
                            И они являются отсчетом для дальнеших зон руд.
                            </p>
                            <img class="img-responsive" src="/assets/img/gregtech/betweenchunk.png">
                            <br>
                            <p>3. Принцип георазведки. Розовым помечены центры смешанных жил, которые могут заспавнится или быть пустыми(зависит от шанса).
                            Красным - помечены территории генерации жил в размере 3 на 3 чанка(зависит от размера). Копать желательно штольнями(вертикалные шахты). Определить дальнейший чанк с рудой, через каждые два чанка от центрального чанка предыдущией руды. </p>
                            <br>
                            <img class="img-responsive" src="/assets/img/gregtech/minevein.png">
                            </div>
                        </div>
                        <div class="margin-8"></div>
                    <div class="panel panel-info">
                        <div class="panel-heading"><div class="sidebar-header">Таблица Ore Mix Vein Gen</div></div>
                            <div class="panel-body post">
                                <p>В этой таблице указаны все руды и их местонахождения</p>
                                <div class="table">
                                    <table class = "table table-bordered table_sort">
                                        <thead>
                                            <tr>
                                                <th>Имя жилы</th>
                                                <th>Минимальная высота</th>
                                                <th>Максимальная высота</th>
                                                <th>Редкость жилы</th>
                                                <th>Плотность жилы</th>
                                                <th>Размер жилы</th>
                                                <th>Overworld</th>
                                                <th>End</th>
                                                <th>Nether</th>
                                                <th>Primary<br>Secondary<br>Inbetween<br>Sporadic</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($OreMixvein as $Mix):?>
                                            <tr>
                                                <td class="leftinfocell"><?php echo $Mix->namevein?></td>
                                                <td><?php echo $Mix->min?></td>
                                                <td><?php echo $Mix->max?></td>
                                                <td><?php echo $Mix->rarity?></td>
                                                <td><?php echo $Mix->density?></td>
                                                <td><?php echo $Mix->size?></td>
                                                <td><?php echo $Mix->overworld?></td>
                                                <td><?php echo $Mix->nether?></td>
                                                <td><?php echo $Mix->end?></td>
                                                <td><?php echo $Mix->nameores?></td>
                                            </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>

                        </div>
                    </div>
                </div>

            </div>
            
        </div>
</div>