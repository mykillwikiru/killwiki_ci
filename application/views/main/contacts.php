                <!--Content LeftBlock START-->
                <div class="col-lg-9">
                    <h1>Как меня найти . . .</h1>

                    <div class="row">
                        <div class="well clearfix">
                            <div class="panel panel-info">
                                <div class="panel-heading vk"><div class="sidebar-header"><a href="https://vk.com/pmorkovnikov"><img src="assets/img/cosial/vk.png"></a></div></div>
                                    <div class="panel-body vcontact">

                                        <p>При переходе на ссылку на мой ВКонтакте, вы можете мне предложить какой-либо мод на рассмотрение или же свое предложение по сайту, ну или просматривать мою жизнь, мало ли там я тоже буду указывать свои действия.
                                        </p>

                                    </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading yt"><div class="sidebar-header"><a href="https://www.youtube.com/channel/UCPFTy7tagXwGSFOwarWH00g?view_as=subscriber"><img src="assets/img/cosial/youtube-logo.png"></a></div></div>
                                    <div class="panel-body youtube">

                                        <p>А при вот этой ссылке, можно перейти на мой YouTube канал, там я снимаю Летсплеи, даже есть записи стримов по сборкам Minecraft и прочие прохождения игр. Сейчас там застой, потому что сейчас я занят сайтом.
                                        </p>

                                    </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading tw"><div class="sidebar-header"><a href="https://www.twitch.tv/killfind2013"><img src="assets/img/cosial/logo_twitch.jpg"></a></div></div>
                                    <div class="panel-body twitch">

                                        <p>А вот по этой ссылке, можно попасть на мой Стрим-канал Twitch, тут можно смотреть на мои стримы, даже записи стримов или Сингл-видео.</p>

                                    </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading da"><div class="sidebar-header"><a href="https://www.donationalerts.com/r/killfind"><img src="assets/img/cosial/DA.png"></a></div></div>
                                    <div class="panel-body donate">

                                        <p>А здесь вы можете помочь моему творчеству, на топливо собрать, или чтоб вам и мне было приятно. Потому что я стараюсь для себя и для вас.</p>

                                    </div>
                            </div>
                            
                                
                
                        </div>
                    </div>
                    <div class="margin-8"></div>
                </div>
                <!--Content RightBlock START-->
                <div class="col-lg-3">
					<?php $this->load->view('templates/menu'); ?>
                </div>