                <!--Content LeftBlock START-->
                <div class="col-lg-9">
                    <h1>Поехали кусать . . .</h1>

                    <div class="row">
                        <div class="well clearfix">
                            <div class="panel panel-info">
                                <div class="panel-heading"><div class="sidebar-header">MODLIST</div></div>
                                    <div class="panel-body modlist">
                                        <div class="row">
                                            <div class="well clearfix">
                                                <div class="col-lg-3 col-md-2 text-center">
                                                <img class="img-thumbnail" src="assets/img/modlist/gt5.png" alt="gregtech">
                                                <p>Gregtech 5</p>
                                                </div>
                                                
                                                <div class="col-lg-9 col-md-10">
                                                <p>
                                                    Gregtech - аддон к моду Industrial Craft, он усложняет рецепты до неузнаваемости, и добавляет сложные и многоуровневые крафты. С помощью него можно усложнить выживание в Minecraft, это получение разных сплавов и металлов для получения машин или предметов высшего Tier, относительно младшего, производство органической и неорганической химии, то есть даст фору вашему мозгу, каждая ошибка научит вас построению Электросети, учитывания Ампера и вольтажа принимающего или отдающего, да соглашусь что данный мод заставляет совершать одни и те же действия, но это сулит продумыванию при апргрейде сети и машинок и систем переработок.
                                                </p>
                                                </div>
                                                
                                                <div class="col-lg-12">
                                                <a href="/gregtech" class="btn btn-lg btn-warning pull-right">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="margin-8"></div>
                                            <div class="well clearfix">
                                                <div class="col-lg-3 col-md-2 text-center">
                                                <img class="img-thumbnail" src="assets/img/modlist/gt++.png" alt="gt++">
                                                <p>GT++</p>
                                                </div>
                                                
                                                <div class="col-lg-9 col-md-10">
                                                <p>
                                                    Мод GT++ (GTplusplus) добавляет нам множество мультиблоков, работающие в режиме одноблока, с при определенном ампераже сингл-блок машины Gregtech, может совершить операцию быстрее по времени, и по количеству обработки подачи. Также он добавляет новые сплавы для производства и построения разных блоков, новые компоненты , а так же ядерную химию.
                                                <br>
                                                <span>В разработке</span>
                                                </p>
                                                </div>
                                                
                                                <div class="col-lg-12">
                                                <a href="/gtplusplus" class="btn btn-lg btn-warning pull-right">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="margin-8"></div>
                                            <div class="well clearfix">
                                                <div class="col-lg-3 col-md-2 text-center">
                                                <img class="img-thumbnail" src="assets/img/modlist/embers.png" alt="embers">
                                                <p>Embers Rekindled</p>
                                                </div>
                                                
                                                <div class="col-lg-9 col-md-10">
                                                <p>
                                                    Embers Rekindled - данный мод является полумагическим и полутехническим. Он добавляет разные типы оружия, брони с возможностью зачарования в специальной наковальне. А чтоб все это вам совершить вам нужно стартовый этап развития, опыт в алхимии и прочих моментов. Данный мод может быть добавлен в ваш модпак для расширения кругозора модов.
                                                </p>
                                                </div>
                                                
                                                <div class="col-lg-12">
                                                <a href="/embers" class="btn btn-lg btn-warning pull-right">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="margin-8"></div>
                                            <div class="well clearfix">
                                                <div class="col-lg-3 col-md-2 text-center">
                                                <img class="img-thumbnail" src="assets/img/modlist/logpipes.png" alt="lp">
                                                <p>Logistic Pipes</p>
                                                </div>
                                                
                                                <div class="col-lg-9 col-md-10">
                                                <p>
                                                    Мод добавляет некоторые механизмы и трубы для создания логистической сети, некая бета Applied Energestics 2. Позволяет пописыванию маршрутов, фильтрации, отправке и приеме жидкостей и предметов. Здесь я указываю основы работы труб и работу логистической сети.
                                                </p>
                                                </div>
                                                
                                                <div class="col-lg-12">
                                                <a href="/logisticpipes" class="btn btn-lg btn-warning pull-right">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="margin-8"></div>
                                            <div class="well clearfix">
                                                <div class="col-lg-3 col-md-2 text-center">
                                                <img class="img-thumbnail" src="assets/img/modlist/rftools.png" alt="lp">
                                                <p>RFTools</p>
                                                </div>
                                                
                                                <div class="col-lg-9 col-md-10">
                                                <p>
                                                    В разработке
                                                </p>
                                                </div>
                                                
                                                <div class="col-lg-12">
                                                <a href="#" class="btn btn-lg btn-warning pull-right">Подробнее</a>
                                                </div>
                                            </div>
                                            <div class="margin-8"></div>
                                            
                                        </div>
                                        
                
    
            
                                    </div>
                            </div>
                            
                                
                
                        </div>
                    </div>
                    <div class="margin-8"></div>
                </div>
                <!--Content RightBlock START-->
                <div class="col-lg-3">
                    <?php $this->load->view('templates/menu'); ?>
                </div>