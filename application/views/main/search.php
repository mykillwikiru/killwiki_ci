<div class="panel panel-info">

    <div class="panel-heading"><div class="sidebar-header">(Найдено <?php echo $tCount; ?>)</div></div>
        <div class="panel-body search">
        <?php echo $pagination; ?>
        <?php foreach ($search_result as $key => $value): ?>
            <div class="panel panel-info">
                <div class="panel-heading"><div class="sidebar-header"><?php echo $value['name']; ?> : [<?php echo $value['modname']; ?>]</div></div>
                <div class="panel-body search_post">
                    <img src="<?php echo $value['imgdir']?>" alt="">
                    <p><?php echo $value['descript'].'<br>'; ?></p>
                    <a href="<?php echo $value['dir']?>" class="btn btn-lg btn-warning pull-right">Подробнее</a>
                </div>
            </div>
        <?php endforeach ?>
    <?php echo $pagination; ?>
</div>