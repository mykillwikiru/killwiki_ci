<!--Content LeftBlock START-->
<div class="col-lg-9">
    <h1>Вникаем в неизведанное</h1>

    <div class="row">
        <div class="well clearfix">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
                <div class="panel panel-info">
                    <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        Электросеть Gregtech 5  
                        </a>
                    </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body modhidden">
                    Особенность проектирования электросети: источник, потребитель, конвертатор напряжения, хранитель энергии и переносчик энергии.
                    <br>
                    <span>Источник</span> - это генераторы, которые являются источниками энергии в сети, <a href="/gregtech/utilitmachinesgt/battery-buffer">Battery Buffer</a>, являющиеся накопителями энергии, а также трансформаторы - конвертаторами энергии из пониженного на повышенный ток или наоборот.
                    <br>
                    <span>Потребитель</span> - может выступать как и любая электромашина, <a href="/gregtech/utilitmachinesgt/battery-buffer">Battery Buffer</a> и <a href="/gregtech/utilitmachinesgt/voltage-transformer">трансформаторов</a>.
                    <br>
                    <span>Конвертатор энергии</span> - может выступать все типы <a href="/gregtech/utilitmachinesgt/voltage-transformer">трансформаторов</a>, которые могут быть и потребителями и источниками в электросети.
                    <br>
                    <span>Хранителями энергии</span> - выступают <a href="/gregtech/utilitmachinesgt/battery-buffer">Battery Buffer</a> с батарейками внутри, которые содержат энергии, для приема или отдачи энергии
                    <br>
                    <span>Переносщики энергии</span> - это могут быть <a href="/gregtech/otheritemsgt/cable">кабеля</a> и <a href="/gregtech/otheritemsgt/wire">провода</a>(но лучше их не использовать), которые переносят в себе энергию в элетросети.
                    <br><br>
                    <h1>Правило построения элетросетей.</h1>
                    <br><br>
                    1. Во всей ветке учитывать <span>напряжение</span>.
                    То есть если есть провод, у которого напряжение HV, нельзя к нему подключать провод низкого напряжения, потому что он сгорит из за переизбытка энергии.
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example1.png">
                    <br>
                    Но к проводу HV нужно подключать провод того же напряжения. 
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example2.png">
                    <br><br>
                    2. <span>Сила тока</span>. Еще одно важное правило, допустим у вас 4 генератора и 1 потребитель
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example3.png">
                    <br>
                    Принцип такой, что количество источников равна количеству ампера провода. В нашем случае 4 амперный провод идет со стороны генераторов в сторону потребителя. 
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example4.png">
                    <br>
                    Но если вы подключите 1 амперный провод то он сгорит, потому что провод не осилит эту силу тока, идущий со стороны источников в два потребителя, а те отдают 512 EU по четыре раза . 
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example5.png">
                    <br>
                    Для наглядности представим провод в виде квадрата, а в нем то количество квадратов, равное количеству источников. То в одном блоке провода может содержатся 4 пакета по 512, которые могут разойтись в сторону потребителей.
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example6.png">
                    <br><br>
                    3. <span>Дальность переноса и потери тока</span>. Итак тут интересней, чем больше расстояние между источником и потребителем, тем меньше придет энергии к потребителю, из-за потерь напряжения в кабеле. Но чем выше напряжение, тем больше можно передать энергии с незначительными потерями энергии.
                    Вот пример, есть буфер у которого есть полные батарейки, а вот потребитель с пустыми батарейками. Провод в нашем случае LV напряжения. При подключении в притык: потери миниальны
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example7.png">
                    <br>
                    При подключении провода в длину 1 блок, то уже напряжение будет равно 31 EU и уже будут потери.
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example8.png">
                    <br>
                    При подключении провда в длину 31 блоков, то напряжение - 1 EU 
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example9.png">
                    <br>
                    Так что в будущем проектируйте базу таким образом, чтоб в начальных этапах развития грега, в области электричества, позаботьтесь об дальности между потребителями и источниками. Так же есть машины и блоки мультиблоков, которые могут запросить 2 ампера или 3 ампера, то тогда вам нужен уже 2x или 4x и тд Кабель соответсвующего напряжения.
                    <br><br>
                    4. <span>Конвертирование энергии</span>> из пониженного в повышенный ток или наоборот. Здесь уже нужно быть внимательным. Вот трансформатор HV->MV это его базовый режим
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example10.png">
                    <br>
                    а вот тот же трансформатор но в повышающем режиме MV->HV
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example11.png">
                    <br>
                    Принцип такой, что при базовом режиме оно принимает одной из сторон повышенный ток при одном ампере тока
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example12.png">
                    <br>
                    , и затем через выход отдает пониженный ток, но при 4 амперах
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example13.png">
                    <br>
                     Это важно!!!
                    А при втором режиме, он через данный выход принимает пониженный ток с 4 амперами тока, и выдает из этих сторон 1 ампер повышенного тока
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example14.png">
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example15.png">
                    <br>
                    А зачем это нужно, спросите вы? Я отвечу, ради минимизации потерь при переносе энергии, если в допустимом случае, вам нужно перенести энергии без потерь из одного конца базы от источника в другой конец базы к потребителю.
                    <br>
                    <br>
                    <h1>Примеры проектирования:</h1>
                    4 генератора и 1 потребитель
                    <br>
                    вариант 1:
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example16.png">
                    <br>
                    вариант 2(лучше и желательно):
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example17.png">
                    <br>
                    4 генератора и 1 потребитель повышенного напряжения с использованием трансформатора.
                    <br>
                    <img class="img-responsive" src="/assets/img/gregtech/electricity/example18.png">
                    <br>
                    </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Установка модов для MultiMC, TwitchApp.
                        </a>
                    </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body modhidden">
                    </div>

                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Создание сервера Minecraft (Vanilla, Modded)
                        </a>
                    </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body modhidden">
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="margin-8"></div>
</div>
<!--Content RightBlock START-->
<div class="col-lg-3">
    <?php $this->load->view('templates/menu'); ?>
</div>