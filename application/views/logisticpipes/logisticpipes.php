<!--Content LeftBlock START-->
<div class="col-lg-12">
                    <h1>Logistic Pipes</h1>
                    

                    <div class="row">
                        <div class="well clearfix">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">

                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                      Установка мода Logistic Pipes
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body modhidden">
                                    <p>
                                    <span>Для Minecraft версии 1.12.2</span>
                                    <br>
                                    <br>
                                    Версия мода при составлении Wiki - - 0.10.2.216
                                    <br>
                                    <a href="https://www.curseforge.com/minecraft/mc-mods/logistics-pipes/files/2850338">Скачать мод</a>
                                    <br>
                                    Требуемый <a href= "http://http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html" >Forge 14.23.5.2847</a>
                                    <br>
                                    Требуется <a href="https://www.curseforge.com/minecraft/mc-mods/codechicken-lib-1-8/files/2779848">CodeChikenLib</a>
                                    <br>
                                    Требуется <a href="https://www.curseforge.com/minecraft/mc-mods/shadowfacts-forgelin/files/2785465">ForgeLin</a>
                                    <br>
                                    Мод для работы с данной модификацией <a href="https://www.curseforge.com/minecraft/mc-mods/buildcraft/files/2836717">BuildCraft</a>
                                    <br>
                                    Скаченный мод поместить в папку mods вашей сборки
                                    <br>
                                    При установке в MultiMC
                                    <br>
                                    1. Редактировать сборку
                                    <br>
                                    2. Вкладка "Модификации
                                    <br>
                                    3. Тыкнуть "Добавить
                                </p>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                      Предметные Logistic Pipes
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя трубы</th>
                                                <th>Описание</th>
                                            </tr>
                                            <?php foreach($itempipe as $itemlp):?>
                                            <tr>
                                                <td><img src="<?php echo $itemlp->imgdir;?>" alt=""></td>
                                                <td><a href="<?php echo $itemlp->dir;?>"><?php echo $itemlp->name;?></a></td>
                                                <td><?php echo $itemlp->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                            
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Жидкостные Logistic Pipes
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя трубы</th>
                                                <th>Описание</th>
                                            </tr>
                                            <?php foreach($fluidpipe as $fluidlp):?>
                                            <tr>
                                                <td><img src="<?php echo $fluidlp->imgdir;?>" alt=""></td>
                                                <td><a href="/logisticpipes/itempipeLP/<?php echo $fluidlp->slug;?>"><?php echo $fluidlp->name;?></a></td>
                                                <td><?php echo $fluidlp->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                            
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFour">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                      Предметы и инструменты
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя предмета/инструмента</th>
                                                <th>Описание предмета/инструмента</th>
                                            </tr>
                                            <?php foreach($itemandtools as $itemandtoolslp):?>
                                            <tr>
                                                <td><img src="<?php echo $itemandtoolslp->imgdir;?>" alt=""></td>
                                                <td><a href="/logisticpipes/itemandtoolsLP/<?php echo $itemandtoolslp->slug;?>"><?php echo $itemandtoolslp->name;?></a></td>
                                                <td><?php echo $itemandtoolslp->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                            
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFive">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                      Блоки Logistic Pipes
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя блока</th>
                                                <th>Описание блока</th>
                                            </tr>
                                            <?php foreach($blocks as $blockslp):?>
                                            <tr>
                                                <td><img src="<?php echo $blockslp->imgdir;?>" alt=""></td>
                                                <td><a href="/logisticpipes/blocksLP/<?php echo $blockslp->slug;?>"><?php echo $blockslp->name;?></a></td>
                                                <td><?php echo $blockslp->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                            
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSix">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                      Модули Logistic Pipes
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя модуля</th>
                                                <th>Описание модуля</th>
                                            </tr>
                                            <?php foreach($module as $modulelp):?>
                                            <tr>
                                                <td><img src="<?php echo $modulelp->imgdir;?>" alt=""></td>
                                                <td><a href="/logisticpipes/moduleLP/<?php echo $modulelp->slug;?>"><?php echo $modulelp->name;?></a></td>
                                                <td><?php echo $modulelp->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                            
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSeven">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                      Модернизаторы Logistic Pipes
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя модернизатора</th>
                                                <th>Описание модернизатора</th>
                                            </tr>
                                            <?php foreach($upgrade as $upgradelp):?>
                                            <tr>
                                                <td><img src="<?php echo $upgradelp->imgdir;?>" alt=""></td>
                                                <td><a href="/logisticpipes/upgradeLP/<?php echo $upgradelp->slug;?>"><?php echo $upgradelp->name;?></a></td>
                                                <td><?php echo $upgradelp->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                            
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>                                                                                                                                                    

                            </div>
                            
                                
                
                        </div>
                    </div>
                    <div class="margin-8"></div>
                </div>