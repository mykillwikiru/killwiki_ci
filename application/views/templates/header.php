<!DOCTYPE html>
<html lang="ru">
	<head>
		<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-149283726-1"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-149283726-1');
		</script>
		<meta charset="utf-8"> -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" 
		content="<?php echo $keywords=mb_strtolower($title); ?>, <?php echo $title; ?>, wiki, mykillwiki, вики, майнкрафт, minecraft, 1.7.10, 1.12.2">
		
		<title><?php echo $title; ?></title>

		<!-- Bootstrap -->
		<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		

	</head>
	<body>
	
  	<!--Header Start-->
    <div class="container-fluid">
		<div class="row">
    		<nav role="navigation" class="navbar navbar-inverse">
    			<div class="container">

    				<div class="navbar-header header">
    					<div class="container">
    						<div class="row">
    							<div class="col-lg-12">
    								<h1><a href="/">KILLFIND_WIKI</a></h1>
    								<h2>Кусь за знания!</h2>
    							</div>
    						</div>
    					</div>
<!--Button for mobile-->
    					<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">

    						<span class="sr-only">Toggle avigation</span>
    						<span class="icon-bar"></span>
    						<span class="icon-bar"></span>
    						<span class="icon-bar"></span>
    						
    					</button>
<!--Button for mobile-->
    				</div>

<!--menu start-->					
						<div id="navbarCollapse" class="collapse navbar-collapse navbar-left">
							<ul class= "nav nav-pills">
								<li <?php echo show_active_menu(0) ?> ><a href="/">Главная</a></li>
								<li <?php echo show_active_menu('wiki') ?> ><a href="/wiki">Wiki</a></li>
								<li <?php echo show_active_menu('guide') ?> ><a href="/guide">Гайды</a></li>
								<li <?php echo show_active_menu('contacts') ?> ><a href="/contacts">Контакты</a></li>
								
							</ul>
						</div>
                        <form role="search" class="visible-xs visible-sm visible-md visible-lg" action="/search/" method="get">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="search" name="q_search" class="form-control input-lg" placeholder="Ваш запрос">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default btn-lg" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
<!--menu end-->
    			</div>
    		</nav>
    	</div>
    </div>
	<!--Header END-->
    <!--Content START-->
    <div class="wrapper content">
    	<div class="container">
            <div class="row">