<!--Content LeftBlock START-->
<div class="col-lg-12">
                    <h1>RFTools</h1>
                    

                    <div class="row">
                        <div class="well clearfix">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">

                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                      Установка мода RFTools
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body modhidden">
                                    <p>
                                    <span>Для Minecraft версии 1.12.2</span>
                                    <br>
                                    <br>
                                    Версия мода при составлении Wiki - - RFTools 1.12-7.73
                                    <br>
                                    <a href="https://www.curseforge.com/minecraft/mc-mods/rftools/files/2861573">Скачать мод</a>
                                    <br>
                                    Требуемый <a href= "http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.12.2.html" >Forge 14.23.5.2847</a>
                                    <br>
                                    Требуемый <a href= "https://www.curseforge.com/minecraft/mc-mods/mcjtylib/files/2745846" >McJtyLib</a>
                                    <br>
                                    Скаченный моды поместить в папку mods вашей сборки
                                    <br>
                                    При установке в MultiMC
                                    <br>
                                    1. Редактировать сборку
                                    <br>
                                    2. Вкладка "Модификации
                                    <br>
                                    3. Тыкнуть "Добавить
                                </p>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                      Материалы
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя материала</th>
                                            </tr>
                                            <?php foreach($materialRFT as $matrft):?>
                                            <tr>
                                                <td><img src="<?php echo $matrft->imgdir;?>" alt=""></td>
                                                <td><a href="<?php echo $matrft->dir;?>"><?php echo $matrft->name;?></a></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Инструменты
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя инструмента</th>
                                                <th>Описание инструмента</th>
                                            </tr>
                                            <?php foreach($toolsRFT as $tool):?>
                                            <tr>
                                                <td><img src="<?php echo $tool->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $tool->dir;?>"><?php echo $tool->name;?></a></td>
                                                <td><?php echo $tool->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFour">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                      Мелкие машины
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя машины</th>
                                                <th>Описание машины</th>
                                            </tr>
                                            <?php foreach($smallmachineRFT as $smallmach):?>
                                            <tr>
                                                <td><img src="<?php echo $smallmach->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $smallmach->dir;?>"><?php echo $smallmach->name;?></a></td>
                                                <td><?php echo $smallmach->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingFive">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                      Модули с эффектами
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя модуля</th>
                                                <th>Описание модуля</th>
                                            </tr>
                                            <?php foreach($enviromentalmoduleRFT as $envmodule):?>
                                            <tr>
                                                <td><img src="<?php echo $envmodule->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $envmodule->dir;?>"><?php echo $envmodule->name;?></a></td>
                                                <td><?php echo $envmodule->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSix">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                      Модули хранения
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя модуля</th>
                                                <th>Описание модуля</th>
                                            </tr>
                                            <?php foreach($storagemoduleRFT as $stormodule):?>
                                            <tr>
                                                <td><img src="<?php echo $stormodule->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $stormodule->dir;?>"><?php echo $stormodule->name;?></a></td>
                                                <td><?php echo $stormodule->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingSeven">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                      Модули отображения
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя модуля</th>
                                                <th>Описание модуля</th>
                                            </tr>
                                            <?php foreach($screenmoduleRFT as $screenmodule):?>
                                            <tr>
                                                <td><img src="<?php echo $screenmodule->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $screenmodule->dir;?>"><?php echo $screenmodule->name;?></a></td>
                                                <td><?php echo $screenmodule->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingEight">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                      Модули шаблонов
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя модуля</th>
                                                <th>Название модуля</th>
                                            </tr>
                                            <?php foreach($shapecardRFT as $shapecard):?>
                                            <tr>
                                                <td><img src="<?php echo $shapecard->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $shapecard->dir;?>"><?php echo $shapecard->name;?></a></td>
                                                <td><?php echo $shapecard->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                            
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-info">
                                <div class="panel-heading" role="tab" id="headingNine">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                      Главные блоки RFTools
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                  <div class="panel-body modhidden">
                                    <div class="table">
                                        <table class ="table table-bordered">
                                            <tr>
                                                <th>#</th>
                                                <th>Имя блока</th>
                                                <th>Описание блока</th>
                                            </tr>
                                            <?php foreach($machineRFT as $machine):?>
                                            <tr>
                                                <td><img src="<?php echo $machine->imgdir; ?>" alt=""></td>
                                                <td><a href="<?php echo $machine->dir;?>"><?php echo $machine->name;?></a></td>
                                                <td><?php echo $machine->descript;?></td>
                                            </tr>
                                            <?php endforeach?>
                                        </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              
                              

                              



                            </div>
                            
                                
                
                        </div>
                    </div>
                    <div class="margin-8"></div>
                </div>