<?php

    class Rftools_model extends CI_Model {

        public function __construct()
        {
            $this->load->database();
        }
		//Выборка данных по типу мода, для передачи в контроллер
        public function get_materialRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'materialsRFT')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','RFTools')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }

        public function get_toolsRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'toolsRFT')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','RFTools')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        public function get_smallmachineRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'smallmachineRFT')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','RFTools')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        public function get_enviromentalmoduleRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'enviromentalmoduleRFT')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','RFTools')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        public function get_storagemoduleRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'storagemoduleRFT')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','RFTools')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        public function get_screenmoduleRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'screenmoduleRFT')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','RFTools')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        public function get_shapecardRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'shapecardRFT')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','RFTools')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        public function get_machineRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'machineRFT')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','RFTools')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        public function get_tableinRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->get('Table_In_RFTools');
	        return $query->result();
	        }
	        $query = $this->db->get_where('Table_In_RFTools', array('slug' => $slug));
			return $query->result();
		}
		public function get_craftRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->get('CraftItemsRFT');
	        return $query->result();
	        }
	        $query = $this->db->get_where('CraftItemsRFT', array('slug' => $slug));
			return $query->result();
		}
		public function get_interfaceRFT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->get('InterfaceRFT');
	        return $query->result();
	        }
	        $query = $this->db->get_where('InterfaceRFT', array('slug' => $slug));
			return $query->result();
		}
    }


?>