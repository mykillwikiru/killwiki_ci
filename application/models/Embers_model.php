<?php

    class Embers_model extends CI_Model {
        public function __construct()
        {
            $this->load->database();
        }

        public function get_oresEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'oregenember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        
        public function get_toolsEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'toolsember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        
        public function get_advtoolsEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'advtoolsember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }

        public function get_transportEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'transportember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }

        public function get_energycellEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'energycellember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		public function get_otheritemsEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'otheritemsember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		public function get_modifiersEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'modifiersember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		public function get_mainblockEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'mainblockember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		public function get_alchemyEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'alchemyember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		public function get_armorEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'armorember')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Embers')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		public function get_tableinEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->get('TableInEmber');
	        return $query->result();
	        }
	        $query = $this->db->get_where('TableInEmber', array('slug' => $slug));
			return $query->result();
		}
		
		public function get_craftEmbers($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->get('CraftItemsEmber');
	        return $query->result();
	        }
	        $query = $this->db->get_where('CraftItemsEmber', array('slug' => $slug));
			return $query->result();
		}
		

        
        
        
    }

?>