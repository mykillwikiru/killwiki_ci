<?php

	class Greg_model extends CI_Model
	{
		public function __construct()
        {
                $this->load->database();
        }
//Погрузка модели статичной страницы toolsgt
		public function get_toolsGT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'tools')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
	
//Погрузка модели статичной страницы advtoolsgt
    	public function get_advtoolsGT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'advtools')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
//Погрузка макета статичной страницы электроинструментов
    	public function get_electoolsGT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'electools')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
    	
//Подгрузка Мультиблоков
		public function get_multiblocksGT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'Multiblock')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}

		//метод для получения всех блоков сборки(При $slug(BDmods) = $name(MultiblockGTAssembly))
		public function get_multiblockassemblygt($slug = FALSE) {
			if ($slug === FALSE) {
	        	$query = $this->db->
		        get('MultiblockGTAssembly');
	        return $query->result();
	        }
	        $query = $this->db->get_where('MultiblockGTAssembly', array('name' => $slug));
			return $query->result();
		}
		
//START BlockForMultiBlock
		public function get_blockformultiblockGT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'Blockformultiblock')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
//Start SteamAge
    	public function get_bronzeageGT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'bronzeage')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		public function get_hpageGT($slug = FALSE) {
	        if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'HP-Age')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}

//Start BatteryGT
		public function get_batteryGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				where('typeinmod', 'battery')->
				get('BDmods');
				return $query->result();
			}
			$query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}

		

//Start Machine component  	
		public function get_machcompGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				where('typeinmod', 'machcomp')->
				get('BDmods');
				return $query->result();
			}
			$query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}

// Start Other Items
    	
		public function get_otheritemsGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				where('typeinmod', 'otheritems')->
				get('BDmods');
				return $query->result();
			}
			$query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}


//START Circuits GT
		public function get_circuitsGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				where('typeinmod', 'circuits')->
				get('BDmods');
				return $query->result();
			}
			$query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}


//Start Electromachines
		public function get_electromachinesGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				where('typeinmod', 'electromachines')->
				get('BDmods');
				return $query->result();
			}
			$query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
//START Utilit Machines
		public function get_utilmachinesGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				where('typeinmod', 'utilmachines')->
				get('BDmods');
				return $query->result();
			}
			$query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		

//START Armor
		public function get_armorGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				where('typeinmod', 'armorgt')->
				get('BDmods');
				return $query->result();
			}
			$query = $this->db->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}

//END подразделов

		//подключение к общей БД для данных в таблицу под картинкой
		public function get_tableinGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				get('TableInGT');
				return $query->result();
			}
			$query = $this->db->get_where('TableInGT', array('slug' => $slug));
			return $query->result();
		}
		//подключение к общей БД для данных интерфейсов машин
		public function get_interfaceGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				get('InterfaceGT');
				return $query->result();
			}
			$query = $this->db->get_where('InterfaceGT', array('slug' => $slug));
			return $query->result();
		}

		//подключение к общей БД для данных крафтов всех машин и предметов
		public function get_craftItemsGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				get('CraftItemsGT');
				return $query->result();
			}
			$query = $this->db->get_where('CraftItemsGT', array('slug' => $slug));
			return $query->result();
		}
		//подключение к общей БД для данных значений всех машин и предметов
		public function get_valueItemsGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				get('ValueItemsGT');
				return $query->result();
			}
			$query = $this->db->get_where('ValueItemsGT', array('slug' => $slug));
			return $query->result();
		}
		//подключение к БД материалов обычных инструментов
		public function get_materialToolsGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				get('MaterialToolsGT');
				return $query->result();
			}
			$query = $this->db->get_where('MaterialToolsGT', array('slug' => $slug));
			return $query->result();
		}
		//подключение к БД материалов продвинутых инструментов
		public function get_materialAdvToolsGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				get('MaterialAdvToolsGT');
				return $query->result();
			}
			$query = $this->db->get_where('MaterialAdvToolsGT', array('slug' => $slug));
			return $query->result();
		}
		//подключение к БД материалов электро-инструментов
		public function get_materialElecToolsGT($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				get('MaterialElecToolsGT');
				return $query->result();
			}
			$query = $this->db->get_where('MaterialElecToolsGT', array('slug' => $slug));
			return $query->result();
		}

		//Спец метод для взятия данных из БД в отдельную страницу rotorgt
		public function get_rotorgt() {
			$query = $this->db->get('TurbineRotor');
			return $query->result();
		}

		public function get_materialrotorgt() {
			$query = $this->db->order_by('material', 'ASC')->get('TurbineRotor');
			return $query->result();
		}

		public function get_plasmagt() {
			$query = $this->db->order_by('name', 'ASC')->get('PlasmaValue');
			return $query->result();
		}

		public function get_gasgt() {
			$query = $this->db->order_by('name', 'ASC')->get('GasValue');
			return $query->result();
		}
		//gen Ores
		public function get_smallOreGengt() {
			$query = $this->db->get('SmallOresGenGT');
			return $query->result();
		}

		public function get_oreMixVeinGengt() {
			$query = $this->db->get('OreMixVeinGenGT');
			return $query->result();
		}

		//метод для получения рецептов AssemblyLine, все проверки 
		public function get_asscraftGT($slug = FALSE) {
			switch ($slug) {
				case 'fusion-reactor':
					$query = $this->db->
					where('name', 'fusion-reactor')->
					get('AssemblyLineCraftTable');
					return $query->result();

					case 'ultimate-battery':
					$query = $this->db->
					where('name', 'ultimate-battery')->
					get('AssemblyLineCraftTable');
					return $query->result();

					case 'electric-motor':
					$query = $this->db->
					where('name', 'electric-motor')->
					get('AssemblyLineCraftTable');
					return $query->result();

					case 'conveyor-module':
					$query = $this->db->
					where('name', 'conveyor-module')->
					get('AssemblyLineCraftTable');
					return $query->result();

					case 'electric-piston':
					$query = $this->db->
					where('name', 'electric-piston')->
					get('AssemblyLineCraftTable');
					return $query->result();

					case 'robot-arm':
					$query = $this->db->
					where('name', 'robot-arm')->
					get('AssemblyLineCraftTable');
					return $query->result();

					case 'field-generator':
					$query = $this->db->
					where('name', 'field-generator')->
					get('AssemblyLineCraftTable');
					return $query->result();

					case 'emitter':
					$query = $this->db->
					where('name', 'emitter')->
					get('AssemblyLineCraftTable');
					return $query->result();

					case 'sensor':
					$query = $this->db->
					where('name', 'sensor')->
					get('AssemblyLineCraftTable');
					return $query->result();
				
				default:
					$query = $this->db->
					where('name', 'none')->
					get('AssemblyLineCraftTable');
	        		return $query->result();
			}
		}
		
	}


?>