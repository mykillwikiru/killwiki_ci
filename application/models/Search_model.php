<?php
    class Search_model extends CI_Model {
        public function search($q, $row_count, $offset) {
            
                    $array_search = array(
            
                        'name' => $q, 
                        'long_desc' => $q
                        
                    );
            
                    $query = $this->db
            //параметр передачи частичного запроса в массив
                        ->or_like($array_search)
                        ->get('BDmods', $row_count, $offset);
            
                    return $query->result_array();
                }
    }
?>