<?php
    class Gtplusplus_model extends CI_Model {
        
        public function __construct()
            {
                $this->load->database();
                $this->load->model('Gtplusplus_model');
            }
            public function get_multiblockGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    where('typeinmod', 'multiblockGTPP')->
                    get('BDmods');
                return $query->result();
                }
                $query = $this->db->where('modname','GT++')->get_where('BDmods', array('slug' => $slug));
                return $query->row_array();
            }

            public function get_simplemachGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    where('typeinmod', 'simplemachGTPP')->
                    get('BDmods');
                return $query->result();
                }
                $query = $this->db->where('modname','GT++')->get_where('BDmods', array('slug' => $slug));
                return $query->row_array();
            }

            public function get_utilmachGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    where('typeinmod', 'utilmachGTPP')->
                    get('BDmods');
                return $query->result();
                }
                $query = $this->db->where('modname','GT++')->get_where('BDmods', array('slug' => $slug));
                return $query->row_array();
            }
            public function get_machcompGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    where('typeinmod', 'machcompGTPP')->
                    get('BDmods');
                return $query->result();
                }
                $query = $this->db->where('modname','GT++')->get_where('BDmods', array('slug' => $slug));
                return $query->row_array();
            }

            public function get_utilblockformultiGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    where('typeinmod', 'utilblockformultiGTPP')->
                    get('BDmods');
                return $query->result();
                }
                $query = $this->db->where('modname','GT++')->get_where('BDmods', array('slug' => $slug));
                return $query->row_array();
            }
            
            public function get_toolsGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    where('typeinmod', 'toolsGTPP')->
                    get('BDmods');
                return $query->result();
                }
                $query = $this->db->where('modname','GT++')->get_where('BDmods', array('slug' => $slug));
                return $query->row_array();
            }
            
            //подключение к общей БД для данных крафтов всех машин и предметов
            public function get_craftDefGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    get('CraftDefGTPP');
                    return $query->result();
                }
                $query = $this->db->get_where('CraftDefGTPP', array('slug' => $slug));
                return $query->result();
            }
            public function get_craftAssGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    get('CraftAssGTPP');
                    return $query->result();
                }
                $query = $this->db->get_where('CraftAssGTPP', array('slug' => $slug));
                return $query->result();
            }
            public function get_craftScanGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    get('CraftScanGTPP');
                    return $query->result();
                }
                $query = $this->db->get_where('CraftScanGTPP', array('slug' => $slug));
                return $query->result();
            }
            public function get_craftAsslineGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    get('CraftAsslineGTPP');
                    return $query->result();
                }
                $query = $this->db->get_where('CraftAsslineGTPP', array('slug' => $slug));
                return $query->result();
            }
            //подключение к общей БД для данных интерфейсов всех машин и предметов
            public function get_interfaceGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    get('InterfaceGTPP');
                    return $query->result();
                }
                $query = $this->db->get_where('InterfaceGTPP', array('slug' => $slug));
                return $query->result();
            }
            //подключение к БД для данных сборки мультиблоков и как строить
            public function get_assMultiGTPP($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    get('MultiblockGTPPAssembly');
                    return $query->result();
                }
                $query = $this->db->get_where('MultiblockGTPPAssembly', array('name' => $slug));
                return $query->result();
            }
            public function get_tableinGT($slug = FALSE) {
                if ($slug === FALSE) {
                    $query = $this->db->
                    get('TableInGT');
                    return $query->result();
                }
                $query = $this->db->get_where('TableInGT', array('slug' => $slug));
                return $query->result();
            }
            
    }

?>