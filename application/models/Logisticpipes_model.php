<?php

    class Logisticpipes_model extends CI_Model {

        public function __construct() {
            $this->load->database();
        }
        //геттер для предметных труб
        public function get_itempipeLP($slug=FALSE) {
            if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'itempipeLP')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Logistic Pipes')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }
        //геттер для жид труб
        public function get_fluidpipeLP($slug=FALSE) {
            if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'fluidpipeLP')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Logistic Pipes')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }

        //геттер для предметов и инструментов
        public function get_itemandtoolsLP($slug=FALSE) {
            if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'itemandtoolsLP')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Logistic Pipes')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
        }

        //геттер для блоков
        public function get_blocksLP($slug=FALSE) {
            if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'blockLP')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Logistic Pipes')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		//геттер для модулей
        public function get_moduleLP($slug=FALSE) {
            if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'moduleLP')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Logistic Pipes')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		//геттер для апгрейдов
        public function get_upgradeLP($slug=FALSE) {
            if ($slug === FALSE) {
	        	$query = $this->db->
		        where('typeinmod', 'upgradeLP')->
		        get('BDmods');
	        return $query->result();
	        }
	        $query = $this->db->where('modname','Logistic Pipes')->get_where('BDmods', array('slug' => $slug));
			return $query->row_array();
		}
		
		//подключение к общей БД для данных крафтов всех машин и предметов
		public function get_craftItemsLP($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				get('CraftItemsLP');
				return $query->result();
			}
			$query = $this->db->get_where('CraftItemsLP', array('slug' => $slug));
			return $query->result();
		}

		//подключение к общей БД для данных крафтов всех машин и предметов
		public function get_interfaceLP($slug = FALSE) {
			if ($slug === FALSE) {
				$query = $this->db->
				get('InterfaceLP');
				return $query->result();
			}
			$query = $this->db->get_where('InterfaceLP', array('slug' => $slug));
			return $query->result();
		}
    }
    
?>