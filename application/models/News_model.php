<?php

    class News_model extends CI_Model {

        public function __construct()
        {
            $this->load->database();
        }
        //запрос SELECT * FROM `news` ORDER BY `news`.`id` DESC

        public function getNews() {
                $query = $this->db->order_by('id', 'DESC')->get('news');
                return $query->result();
            
        }
    }
?>