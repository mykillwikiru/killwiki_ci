<?php

    class Gtplusplus extends MY_Controller {
        public function __construct(){
            parent::__construct();
            $this->load->model('Gtplusplus_model');         

        }

        public function gtplusplus() {
            //Подгрузка раздела мультиблока, и отображение таблицы
            $gtpp = new Gtplusplus_model;
            
            $data['multiblockGTPP']=$gtpp->get_multiblockGTPP();
            $data['utilblockformultiGTPP']=$gtpp->get_utilblockformultiGTPP();
            $data['simplemachGTPP']=$gtpp->get_simplemachGTPP();
            $data['utilmachGTPP']=$gtpp->get_utilmachGTPP();
            $data['machcompGTPP']=$gtpp->get_machcompGTPP();
            $data['toolsGTPP']=$gtpp->get_toolsGTPP();
            

            $this->data['title'] = "GT++";

            $this->load->view('templates/header', $this->data); //для вывода частей страниц загловка
            $this->load->view('gtplusplus/gtplusplus', $data); // для gtplusplus
            $this->load->view('templates/footer', $this->data); // для футера

        }
        //макет страницы для мультиблоков
        public function multiblockGTPP($slug = NULL) {
            $gtpp = new Gtplusplus_model;
            $data['multiblockGTPP']=$gtpp->get_multiblockGTPP($slug);
            $this->data['title'] = $data['multiblockGTPP']['name'];
			$this->data['name'] = $data['multiblockGTPP']['name'];
			$this->data['img'] = $data['multiblockGTPP']['imgdir'];
			$this->data['desc'] = $data['multiblockGTPP']['long_desc'];
            $data['defgtpp'] = $gtpp->get_craftDefGTPP($slug);
            $data['assgtpp'] = $gtpp->get_craftAssGTPP($slug);
            $data['scangtpp'] = $gtpp->get_craftScanGTPP($slug);
            $data['asslinegtpp'] = $gtpp->get_craftAsslineGTPP($slug);
            $data['multiass'] = $gtpp->get_assMultiGTPP($slug);
            $data['tableinmultiblocks'] = $gtpp->get_tableInGT($slug);
            $data['interfacegtpp'] = $gtpp->get_InterfaceGTPP($slug);

            $this->load->view('templates/header', $this->data); //для вывода частей страниц загловка
            $this->load->view('gtplusplus/multiblockGTPP', $data); // для gtplusplus
            $this->load->view('templates/footer', $this->data); // для футера
        }
        public function utilblockformultiGTPP($slug = NULL) {
            $gtpp = new Gtplusplus_model;
            $data['utilblockformultiGTPP']=$gtpp->get_utilblockformultiGTPP($slug);
            $this->data['title'] = $data['utilblockformultiGTPP']['name'];
			$this->data['name'] = $data['utilblockformultiGTPP']['name'];
			$this->data['img'] = $data['utilblockformultiGTPP']['imgdir'];
			$this->data['desc'] = $data['utilblockformultiGTPP']['long_desc'];
            $data['defgtpp'] = $gtpp->get_craftDefGTPP($slug);
            $data['assgtpp'] = $gtpp->get_craftAssGTPP($slug);
            $data['scangtpp'] = $gtpp->get_craftScanGTPP($slug);
            $data['asslinegtpp'] = $gtpp->get_craftAsslineGTPP($slug);
            $data['tableinubfm'] = $gtpp->get_tableInGT($slug);

            $this->load->view('templates/header', $this->data); //для вывода частей страниц загловка
            $this->load->view('gtplusplus/utilblockformultiGTPP', $data); // для gtplusplus
            $this->load->view('templates/footer', $this->data); // для футера
        }
        public function simplemachGTPP($slug = NULL) {
            $gtpp = new Gtplusplus_model;
            $data['simplemachGTPP']=$gtpp->get_simplemachGTPP($slug);
            $this->data['title'] = $data['simplemachGTPP']['name'];
			$this->data['name'] = $data['simplemachGTPP']['name'];
			$this->data['img'] = $data['simplemachGTPP']['imgdir'];
			$this->data['desc'] = $data['simplemachGTPP']['long_desc'];
            $data['defgtpp'] = $gtpp->get_craftDefGTPP($slug);
            $data['assgtpp'] = $gtpp->get_craftAssGTPP($slug);
            $data['scangtpp'] = $gtpp->get_craftScanGTPP($slug);
            $data['asslinegtpp'] = $gtpp->get_craftAsslineGTPP($slug);
            $data['tableinsimple'] = $gtpp->get_tableInGT($slug);

            $this->load->view('templates/header', $this->data); //для вывода частей страниц загловка
            $this->load->view('gtplusplus/simplemachGTPP', $data); // для gtplusplus
            $this->load->view('templates/footer', $this->data); // для футера
        }
        public function utilmachGTPP($slug = NULL) {
            $gtpp = new Gtplusplus_model;
            $data['utilmachGTPP']=$gtpp->get_utilmachGTPP($slug);
            $this->data['title'] = $data['utilmachGTPP']['name'];
			$this->data['name'] = $data['utilmachGTPP']['name'];
			$this->data['img'] = $data['utilmachGTPP']['imgdir'];
			$this->data['desc'] = $data['utilmachGTPP']['long_desc'];
            $data['defgtpp'] = $gtpp->get_craftDefGTPP($slug);
            $data['assgtpp'] = $gtpp->get_craftAssGTPP($slug);
            $data['scangtpp'] = $gtpp->get_craftScanGTPP($slug);
            $data['asslinegtpp'] = $gtpp->get_craftAsslineGTPP($slug);
            $data['tableinutil'] = $gtpp->get_tableInGT($slug);

            $this->load->view('templates/header', $this->data); //для вывода частей страниц загловка
            $this->load->view('gtplusplus/utilmachGTPP', $data); // для gtplusplus
            $this->load->view('templates/footer', $this->data); // для футера
        }
        public function machcompGTPP($slug = NULL) {
            $gtpp = new Gtplusplus_model;
            $data['machcompGTPP']=$gtpp->get_machcompGTPP($slug);
            $this->data['title'] = $data['machcompGTPP']['name'];
			$this->data['name'] = $data['machcompGTPP']['name'];
			$this->data['img'] = $data['machcompGTPP']['imgdir'];
			$this->data['desc'] = $data['machcompGTPP']['long_desc'];
            $data['defgtpp'] = $gtpp->get_craftDefGTPP($slug);
            $data['assgtpp'] = $gtpp->get_craftAssGTPP($slug);
            $data['scangtpp'] = $gtpp->get_craftScanGTPP($slug);
            $data['asslinegtpp'] = $gtpp->get_craftAsslineGTPP($slug);
            $data['tableinmach'] = $gtpp->get_tableInGT($slug);

            $this->load->view('templates/header', $this->data); //для вывода частей страниц загловка
            $this->load->view('gtplusplus/machcompGTPP', $data); // для gtplusplus
            $this->load->view('templates/footer', $this->data); // для футера
        }

        public function toolsGTPP($slug = NULL) {
            $gtpp = new Gtplusplus_model;
            $data['toolsGTPP']=$gtpp->get_toolsGTPP($slug);
            $this->data['title'] = $data['toolsGTPP']['name'];
			$this->data['name'] = $data['toolsGTPP']['name'];
			$this->data['img'] = $data['toolsGTPP']['imgdir'];
			$this->data['desc'] = $data['toolsGTPP']['long_desc'];
            $data['defgtpp'] = $gtpp->get_craftDefGTPP($slug);
            $data['assgtpp'] = $gtpp->get_craftAssGTPP($slug);
            $data['scangtpp'] = $gtpp->get_craftScanGTPP($slug);
            $data['asslinegtpp'] = $gtpp->get_craftAsslineGTPP($slug);
            $data['tableintools'] = $gtpp->get_tableInGT($slug);

            $this->load->view('templates/header', $this->data); //для вывода частей страниц загловка
            $this->load->view('gtplusplus/toolsGTPP', $data); // для gtplusplus
            $this->load->view('templates/footer', $this->data); // для футера
        }

    }
?>