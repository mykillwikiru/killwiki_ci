<?php
    /*
    ТЕСТОВЫЙ КОД!!!!
    defined('BASEPATH') OR exit('No direc script access allowed');

    капризен к регистру, RFTools.php не канает

    class Rftools extends MY_Controller {
            Создание конструктора
        public function __construct()
        {
            parent::__construct();
        }

        тестовый вывод пустого макета, взятого из /views/rftools/rftools.php
        Прописать путь в маршруте $route['rftools'] = 'rftools/rftools';

        public function rftools() {
            $this->data['title'] = "RFTools";

            $this->load->view('templates/header', $this->data); //для вывода частей страниц загловка
            $this->load->view('rftools/rftools'); // для rftools
            $this->load->view('templates/footer', $this->data); // для футера

        }
        
    } */

    defined('BASEPATH') OR exit('No direc script access allowed');

    class Rftools extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->load->model('Rftools_model');
        }
        //Отображение секторов БД по тип_моду, без привязки к Slug
        public function rftools() {
            $rftmodel = new Rftools_model;
            $data['materialRFT'] = $rftmodel->get_materialRFT();
            $data['toolsRFT'] = $rftmodel->get_toolsRFT();
            $data['smallmachineRFT'] = $rftmodel->get_smallmachineRFT();
            $data['enviromentalmoduleRFT'] = $rftmodel->get_enviromentalmoduleRFT();
            $data['storagemoduleRFT'] = $rftmodel->get_storagemoduleRFT();
            $data['screenmoduleRFT'] = $rftmodel->get_screenmoduleRFT();
            $data['shapecardRFT'] = $rftmodel->get_shapecardRFT();
            $data['machineRFT'] = $rftmodel->get_machineRFT();
            $this->data['title'] = "RFTools";

            $this->load->view('templates/header', $this->data);
            $this->load->view('rftools/rftools', $data);
            $this->load->view('templates/footer', $this->data);


        }
        /* кусок БД с типо_модом материалы, для вставки в шаблон метода materialRFT и
        последовательного отображения и сверки флагов у каждой страницы */
        public function materialRFT($slug = FALSE) {
            $rftmodel = new Rftools_model;
            $data['material'] = $rftmodel->get_materialRFT($slug);
            
            $this->data['title'] = $data['material']['name'];
            $this->data['name'] = $data['material']['name'];
            $this->data['desc'] = $data['material']['long_desc'];
            $this->data['img'] = $data['material']['imgdir'];
            $data['tableinmaterial'] = $rftmodel->get_tableinRFT($slug);
            $data['craftmaterial'] = $rftmodel->get_craftRFT($slug);


            $this->load->view('templates/header', $this->data);
            $this->load->view('rftools/materialRFT', $data);
            $this->load->view('templates/footer', $this->data);
        }

        public function toolsRFT($slug = FALSE) {
            $rftmodel = new Rftools_model;
            $data['tools'] = $rftmodel->get_toolsRFT($slug);

            $this->data['title'] = $data['tools']['name'];
            $this->data['name'] = $data['tools']['name'];
            $this->data['desc'] = $data['tools']['long_desc'];
            $this->data['img'] = $data['tools']['imgdir'];
            $data['tableintools'] = $rftmodel->get_tableinRFT($slug);
            $data['crafttools'] = $rftmodel->get_craftRFT($slug);

            $this->load->view('templates/header', $this->data);
            $this->load->view('rftools/toolsRFT', $data);
            $this->load->view('templates/footer', $this->data);
        }
        public function smallmachineRFT($slug = FALSE) {
            $rftmodel = new Rftools_model;
            $data['smallmach'] = $rftmodel->get_smallmachineRFT($slug);
            $this->data['title'] = $data['smallmach']['name'];
            $this->data['name'] = $data['smallmach']['name'];
            $this->data['desc'] = $data['smallmach']['long_desc'];
            $this->data['img'] = $data['smallmach']['imgdir'];
            $data['tableinsmallmach'] = $rftmodel->get_tableinRFT($slug);
            $data['craftsmallmach'] = $rftmodel->get_craftRFT($slug);
            $data['interface_small_mach'] = $rftmodel->get_interfaceRFT($slug);

            $this->load->view('templates/header', $this->data);
            $this->load->view('rftools/smallmachineRFT', $data);
            $this->load->view('templates/footer', $this->data);
        }
        public function enviromentalmoduleRFT($slug = FALSE) {
            $rftmodel = new Rftools_model;
            $data['envmodule'] = $rftmodel->get_enviromentalmoduleRFT($slug);
            $this->data['title'] = $data['envmodule']['name'];
            $this->data['name'] = $data['envmodule']['name'];
            $this->data['desc'] = $data['envmodule']['long_desc'];
            $this->data['img'] = $data['envmodule']['imgdir'];
            $data['tableinenv'] = $rftmodel->get_tableinRFT($slug);
            $data['craftenv'] = $rftmodel->get_craftRFT($slug);

            $this->load->view('templates/header', $this->data);
            $this->load->view('rftools/enviromentalmoduleRFT', $data);
            $this->load->view('templates/footer', $this->data);
        }
        public function storagemoduleRFT($slug = FALSE) {
            $rftmodel = new Rftools_model;
            $data['stormodule'] = $rftmodel->get_storagemoduleRFT($slug);
            $this->data['title'] = $data['stormodule']['name'];
            $this->data['name'] = $data['stormodule']['name'];
            $this->data['desc'] = $data['stormodule']['long_desc'];
            $this->data['img'] = $data['stormodule']['imgdir'];
            $data['tableinstorage'] = $rftmodel->get_tableinRFT($slug);
            $data['craftstorage'] = $rftmodel->get_craftRFT($slug);

            $this->load->view('templates/header', $this->data);
            $this->load->view('rftools/storagemoduleRFT', $data);
            $this->load->view('templates/footer', $this->data);
        }
        public function screenmoduleRFT($slug = FALSE) {
            $rftmodel = new Rftools_model;
            $data['screenmodule'] = $rftmodel->get_screenmoduleRFT($slug);
            $this->data['title'] = $data['screenmodule']['name'];
            $this->data['name'] = $data['screenmodule']['name'];
            $this->data['desc'] = $data['screenmodule']['long_desc'];
            $this->data['img'] = $data['screenmodule']['imgdir'];
            $data['tableinscreen'] = $rftmodel->get_tableinRFT($slug);
            $data['craftscreen'] = $rftmodel->get_craftRFT($slug);

            $this->load->view('templates/header', $this->data);
            $this->load->view('rftools/screenmoduleRFT', $data);
            $this->load->view('templates/footer', $this->data);
        }
        public function shapecardRFT($slug = FALSE) {
            $rftmodel = new Rftools_model;
            $data['shapecard'] = $rftmodel->get_shapecardRFT($slug);
            $this->data['title'] = $data['shapecard']['name'];
            $this->data['name'] = $data['shapecard']['name'];
            $this->data['desc'] = $data['shapecard']['long_desc'];
            $this->data['img'] = $data['shapecard']['imgdir'];
            $data['tableinshape'] = $rftmodel->get_tableinRFT($slug);
            $data['craftshape'] = $rftmodel->get_craftRFT($slug);

            $this->load->view('templates/header', $this->data);
            $this->load->view('rftools/shapecardRFT', $data);
            $this->load->view('templates/footer', $this->data);
        }
        public function machineRFT($slug = FALSE) {
            $rftmodel = new Rftools_model;
            $data['machine'] = $rftmodel->get_machineRFT($slug);
            $this->data['title'] = $data['machine']['name'];
            $this->data['name'] = $data['machine']['name'];
            $this->data['desc'] = $data['machine']['long_desc'];
            $this->data['img'] = $data['machine']['imgdir'];
            $data['tableinmachine'] = $rftmodel->get_tableinRFT($slug);
            $data['craftmachine'] = $rftmodel->get_craftRFT($slug);
            $data['interface_mach'] = $rftmodel->get_interfaceRFT($slug);

            $this->load->view('templates/header', $this->data);
            $this->load->view('rftools/machineRFT', $data);
            $this->load->view('templates/footer', $this->data);
        }
    }
?>