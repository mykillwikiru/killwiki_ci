<?php
    defined('BASEPATH') OR exit('No direc script access allowed');

    class Embers extends MY_Controller {

        public function __construct() {
            parent::__construct();
            $this->load->model('Embers_model');
        }

        public function embers() {
            $embermodel= new Embers_model;
            $data['oresEmber'] = $embermodel->get_oresEmbers();
            $data['toolsEmber'] = $embermodel->get_toolsEmbers();
            $data['advtoolsEmber'] = $embermodel->get_advtoolsEmbers();
            $data['transportEmber'] = $embermodel->get_transportEmbers();
            $data['energycellEmber'] = $embermodel->get_energycellEmbers();
			$data['otheritemsEmber'] = $embermodel->get_otheritemsEmbers();
			$data['modifiersEmber'] = $embermodel->get_modifiersEmbers();
			$data['mainblockEmber'] = $embermodel->get_mainblockEmbers();
			$data['alchemyEmber'] = $embermodel->get_alchemyEmbers();
			$data['armorEmber'] = $embermodel->get_armorEmbers();

            $this->data['title'] = "Embers Rekindled";
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/embers', $data);
			$this->load->view('templates/footer', $this->data);
        }

        public function oregenember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['ores'] = $embermodel->get_oresEmbers($slug);
            $this->data['title'] = $data['ores']['name'];
            $this->data['name'] = $data['ores']['name'];
			$this->data['img'] = $data['ores']['imgdir'];
			$this->data['desc'] = $data['ores']['long_desc'];
			$data['tableinore'] = $embermodel->get_tableinEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/oregenember', $data);
			$this->load->view('templates/footer', $this->data);
        }

        public function toolsember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['toolsEmber'] = $embermodel->get_toolsEmbers($slug);
            $this->data['title'] = $data['toolsEmber']['name'];
            $this->data['name'] = $data['toolsEmber']['name'];
			$this->data['img'] = $data['toolsEmber']['imgdir'];
			$this->data['desc'] = $data['toolsEmber']['long_desc'];
			$data['tableintools'] = $embermodel->get_tableinEmbers($slug);
			$data['crafttools'] = $embermodel->get_craftEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/toolsember', $data);
			$this->load->view('templates/footer', $this->data);
        }

        public function advtoolsember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['advtoolsEmber'] = $embermodel->get_advtoolsEmbers($slug);
            $this->data['title'] = $data['advtoolsEmber']['name'];
            $this->data['name'] = $data['advtoolsEmber']['name'];
			$this->data['img'] = $data['advtoolsEmber']['imgdir'];
			$this->data['desc'] = $data['advtoolsEmber']['long_desc'];
			$data['tableinadvtools'] = $embermodel->get_tableinEmbers($slug);
			$data['craftadvtools'] = $embermodel->get_craftEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/advtoolsember', $data);
			$this->load->view('templates/footer', $this->data);
        }

        public function transportember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['transportEmber'] = $embermodel->get_transportEmbers($slug);
            $this->data['title'] = $data['transportEmber']['name'];
            $this->data['name'] = $data['transportEmber']['name'];
			$this->data['img'] = $data['transportEmber']['imgdir'];
			$this->data['desc'] = $data['transportEmber']['long_desc'];
			$data['tableintransport'] = $embermodel->get_tableinEmbers($slug);
			$data['crafttransport'] = $embermodel->get_craftEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/transportember', $data);
			$this->load->view('templates/footer', $this->data);
        }
        
        public function energycellember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['energycellEmber'] = $embermodel->get_energycellEmbers($slug);
            $this->data['title'] = $data['energycellEmber']['name'];
            $this->data['name'] = $data['energycellEmber']['name'];
			$this->data['img'] = $data['energycellEmber']['imgdir'];
			$this->data['desc'] = $data['energycellEmber']['long_desc'];
			$data['tableincells'] = $embermodel->get_tableinEmbers($slug);
			$data['craftcell'] = $embermodel->get_craftEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/energycellember', $data);
			$this->load->view('templates/footer', $this->data);
		}
		
		public function otheritemsember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['otheritemsEmber'] = $embermodel->get_otheritemsEmbers($slug);
            $this->data['title'] = $data['otheritemsEmber']['name'];
            $this->data['name'] = $data['otheritemsEmber']['name'];
			$this->data['img'] = $data['otheritemsEmber']['imgdir'];
			$this->data['desc'] = $data['otheritemsEmber']['long_desc'];
			$data['tableinother'] = $embermodel->get_tableinEmbers($slug);
			$data['craftother'] = $embermodel->get_craftEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/otheritemsember', $data);
			$this->load->view('templates/footer', $this->data);
		}
		
		public function modifiersember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['modifiersEmber'] = $embermodel->get_modifiersEmbers($slug);
            $this->data['title'] = $data['modifiersEmber']['name'];
            $this->data['name'] = $data['modifiersEmber']['name'];
			$this->data['img'] = $data['modifiersEmber']['imgdir'];
			$this->data['desc'] = $data['modifiersEmber']['long_desc'];
			$data['tableinmodifier'] = $embermodel->get_tableinEmbers($slug);
			$data['craftmod'] = $embermodel->get_craftEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/modifiersember', $data);
			$this->load->view('templates/footer', $this->data);
		}
		
		public function mainblockember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['mainblockEmber'] = $embermodel->get_mainblockEmbers($slug);
            $this->data['title'] = $data['mainblockEmber']['name'];
            $this->data['name'] = $data['mainblockEmber']['name'];
			$this->data['img'] = $data['mainblockEmber']['imgdir'];
			$this->data['desc'] = $data['mainblockEmber']['long_desc'];
			$data['tableinmainblock'] = $embermodel->get_tableinEmbers($slug);
			$data['crafmainblock'] = $embermodel->get_craftEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/mainblockember', $data);
			$this->load->view('templates/footer', $this->data);
		}
		
		public function alchemyember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['alchemyEmber'] = $embermodel->get_alchemyEmbers($slug);
            $this->data['title'] = $data['alchemyEmber']['name'];
            $this->data['name'] = $data['alchemyEmber']['name'];
			$this->data['img'] = $data['alchemyEmber']['imgdir'];
			$this->data['desc'] = $data['alchemyEmber']['long_desc'];
			$data['tableinalchemy'] = $embermodel->get_tableinEmbers($slug);
			$data['craftalc'] = $embermodel->get_craftEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/alchemyember', $data);
			$this->load->view('templates/footer', $this->data);
		}
		
		public function armorember($slug = FALSE) {
            $embermodel= new Embers_model;
			$data['armorEmber'] = $embermodel->get_armorEmbers($slug);
            $this->data['title'] = $data['armorEmber']['name'];
            $this->data['name'] = $data['armorEmber']['name'];
			$this->data['img'] = $data['armorEmber']['imgdir'];
			$this->data['desc'] = $data['armorEmber']['long_desc'];
			$data['tableinarmor'] = $embermodel->get_tableinEmbers($slug);
			$data['craftarmor'] = $embermodel->get_craftEmbers($slug);
			
			$this->load->view('templates/header', $this->data);
			$this->load->view('embers/armorember', $data);
			$this->load->view('templates/footer', $this->data);
        }
    }
?>