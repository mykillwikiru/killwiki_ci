<?php 
	defined('BASEPATH') OR exit('No direc script access allowed');

	/**
	 * Контроллер вывода инфы во view/gregtech/(:/any)
	 */
	class Gregtech extends MY_Controller {
		//создаем конструктор для класса из родительского
		public function __construct(){
			parent::__construct();
			$this->load->model('Greg_model');         

		}
		//подгрузка вида для всей страницы Gregtech
		public function gregtech() {
			$greg = new Greg_model;
			$data['tools']=$greg->get_toolsGT();
			$data['advtools']=$greg->get_advtoolsGT();
			$data['electools']=$greg->get_electoolsGT();
			$data['multiblocks']=$greg->get_multiblocksGT();
			$data['Blockformultiblock']=$greg->get_BlockformultiblockGT();
			$data['bronzeage']=$greg->get_bronzeageGT();
			$data['hpage']=$greg->get_hpageGT();
			$data['battery'] = $greg->get_batteryGT();
			$data['machcomp']=$greg->get_machcompGT();
			$data['otheritems']=$greg->get_otheritemsGT();
			$data['circuits']=$greg->get_circuitsGT();
			$data['electromachines']=$greg->get_electromachinesGT();
			$data['utilmachines']=$greg->get_utilmachinesGT();
			$data['armor']=$greg->get_armorGT();
			

			$this->data['title'] = "Gregtech 5";
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/gregtech', $data);
			$this->load->view('templates/footer', $this->data);

		}
		//подгрузка макета страниц для инструметов GT
		public function toolsGT($slug = NULL) {
			$greg = new Greg_model;
			$data['tools'] = $greg->get_toolsGT($slug);
			$this->data['title'] = $data['tools']['name'];
			$this->data['name'] = $data['tools']['name'];
			$this->data['img'] = $data['tools']['imgdir'];
			$this->data['desc'] = $data['tools']['long_desc'];
			$data['crafttools'] = $greg->get_craftItemsGT($slug);
			$data['Material'] = $greg->get_materialToolsGT($slug);
			$data['tableintools'] = $greg->get_tableInGT($slug);
			//подгрузка для вьюшки
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/toolsgt', $data);
			$this->load->view('templates/footer', $this->data);
		}
		
		public function advtoolsgt($slug = NULL) {
			$greg = new Greg_model;
			$data['advtools'] = $greg->get_advtoolsGT($slug);
			$this->data['title'] = $data['advtools']['name'];
			$this->data['name'] = $data['advtools']['name'];
			$this->data['img'] = $data['advtools']['imgdir'];
			$this->data['desc'] = $data['advtools']['long_desc'];
			$data['craftadvtools'] = $greg->get_craftItemsGT($slug);
			$data['Material'] = $greg->get_materialAdvToolsGT($slug);
			$data['tableinadvtools'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/advtoolsgt', $data);
			$this->load->view('templates/footer', $this->data);

		}
		public function electoolsgt($slug = NULL) {
			$greg = new Greg_model;
			$data['electools'] = $greg->get_electoolsGT($slug);
			$this->data['title'] = $data['electools']['name'];
			$this->data['name'] = $data['electools']['name'];
			$this->data['img'] = $data['electools']['imgdir'];
			$this->data['desc'] = $data['electools']['long_desc'];
			$data['craftelectrotools'] = $greg->get_craftItemsGT($slug);
			$data['Material'] = $greg->get_materialElecToolsGT($slug);
			$data['tableinelectools'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/electoolsgt', $data);
			$this->load->view('templates/footer', $this->data);
			
		}
		public function multiblocksgt($slug = NULL) {
			$greg = new Greg_model;
			$data['multiblocks'] = $greg->get_multiblocksGT($slug);
			$this->data['title'] = $data['multiblocks']['name'];
			$this->data['name'] = $data['multiblocks']['name'];
			$this->data['img'] = $data['multiblocks']['imgdir'];
			$this->data['desc'] = $data['multiblocks']['long_desc'];
			$data['multiblockcraft'] = $greg->get_craftItemsGT($slug);
			$data['multiblockinterface'] = $greg->get_interfaceGT($slug);
			$data['multiblockass'] = $greg->get_multiblockassemblygt($slug);
			$data['assline'] = $greg->get_asscraftGT($slug);
			$data['tableinmultiblocks'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/multiblocksgt', $data);
			$this->load->view('templates/footer', $this->data);
		}
		public function blockformultiblockgt($slug = NULL) {
			$greg = new Greg_model;
			$data['blockformultiblock'] = $greg->get_blockformultiblockGT($slug);
			$this->data['title'] = $data['blockformultiblock']['name'];
			$this->data['name'] = $data['blockformultiblock']['name'];
			$this->data['img'] = $data['blockformultiblock']['imgdir'];
			$this->data['desc'] = $data['blockformultiblock']['long_desc'];
			$data['BlockForMultiCraft'] = $greg->get_craftItemsGT($slug);
			$data['blockformultiblockinterface'] = $greg->get_interfaceGT($slug);
			$data['blockformultivalue'] = $greg->get_valueItemsGT($slug);
			$data['tableinblockformultiblock'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/blockformultiblockgt', $data);
			$this->load->view('templates/footer', $this->data);
		}
		public function bronzeagegt($slug = NULL) {
			$greg = new Greg_model;
			$data['bronzeage'] = $greg->get_bronzeageGT($slug);
			$this->data['title'] = $data['bronzeage']['name'];
			$this->data['name'] = $data['bronzeage']['name'];
			$this->data['img'] = $data['bronzeage']['imgdir'];
			$this->data['desc'] = $data['bronzeage']['long_desc'];
			$data['steam_age_gt_craft'] = $greg->get_craftItemsGT($slug);
			$data['steam_age_gt_interface'] = $greg->get_interfaceGT($slug);
			$data['tablein_steam_age_gt'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/bronzeagegt', $data);
			$this->load->view('templates/footer', $this->data);

		}
		public function hpagegt($slug = NULL) {
			$greg = new Greg_model;
			$data['hp_age'] = $greg->get_hpageGT($slug);
			$this->data['title'] = $data['hp_age']['name'];
			$this->data['name'] = $data['hp_age']['name'];
			$this->data['img'] = $data['hp_age']['imgdir'];
			$this->data['desc'] = $data['hp_age']['long_desc'];
			$data['steam_age_gt_craft'] = $greg->get_craftItemsGT($slug);
			$data['steam_age_gt_interface'] = $greg->get_interfaceGT($slug);
			$data['tablein_steam_age_gt'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/hpagegt', $data);
			$this->load->view('templates/footer', $this->data);

		}
		public function batterygt($slug = NULL) {
			$greg = new Greg_model;
			$data['battery'] = $greg->get_batteryGT($slug);
			$this->data['title'] = $data['battery']['name'];
			$this->data['name'] = $data['battery']['name'];
			$this->data['img'] = $data['battery']['imgdir'];
			$this->data['desc'] = $data['battery']['long_desc'];
			$data['tableinbatterygt'] = $greg->get_tableInGT($slug);
			$data['batterycraft'] = $greg->get_craftItemsGT($slug);
			$data['batteryvalue'] = $greg->get_valueItemsGT($slug);
			$data['assline'] = $greg->get_asscraftGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/batterygt', $data);
			$this->load->view('templates/footer', $this->data);
		}
		public function machinecomponentsgt($slug = NULL) {
			$greg = new Greg_model;
			$data['machcomp'] = $greg->get_machcompGT($slug);
			$this->data['title'] = $data['machcomp']['name'];
			$this->data['name'] = $data['machcomp']['name'];
			$this->data['img'] = $data['machcomp']['imgdir'];
			$this->data['desc'] = $data['machcomp']['long_desc'];
			$data['tableinmachcompgt'] = $greg->get_tableInGT($slug);
			$data['machcompcraft'] = $greg->get_craftItemsGT($slug);
			$data['assline'] = $greg->get_asscraftGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/machinecomponentsgt', $data);
			$this->load->view('templates/footer', $this->data);
		}

		public function otheritemsgt($slug = NULL) {
			$greg = new Greg_model;
			$data['otheritems'] = $greg->get_otheritemsGT($slug);
			$this->data['title'] = $data['otheritems']['name'];
			$this->data['name'] = $data['otheritems']['name'];
			$this->data['img'] = $data['otheritems']['imgdir'];
			$this->data['desc'] = $data['otheritems']['long_desc'];
			$data['tableinotheritemsgt'] = $greg->get_tableInGT($slug);
			$data['otheritemscraft'] = $greg->get_craftItemsGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/otheritemsgt', $data);
			$this->load->view('templates/footer', $this->data);
		}

		public function circuitsgt($slug = NULL) {
			$greg = new Greg_model;
			$data['circuits'] = $greg->get_circuitsGT($slug);
			$this->data['title'] = $data['circuits']['name'];
			$this->data['name'] = $data['circuits']['name'];
			$this->data['img'] = $data['circuits']['imgdir'];
			$this->data['desc'] = $data['circuits']['long_desc'];
			$data['circuitscraft'] = $greg->get_craftItemsGT($slug);
			$data['tableincircuitsgt'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/circuitsgt', $data);
			$this->load->view('templates/footer', $this->data);
		}

		public function utilitmachinesgt($slug = NULL) {
			$greg = new Greg_model;
			$data['utilmach'] = $greg->get_utilmachinesGT($slug);
			$this->data['title'] = $data['utilmach']['name'];
			$this->data['name'] = $data['utilmach']['name'];
			$this->data['img'] = $data['utilmach']['imgdir'];
			$this->data['desc'] = $data['utilmach']['long_desc'];
			$data['utilmachcraft'] = $greg->get_craftItemsGT($slug);
			$data['utilmachvalue'] = $greg->get_valueItemsGT($slug);
			$data['utilmachinterface'] = $greg->get_interfaceGT($slug);
			$data['tableinutilmachgt'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/utilitmachinesgt', $data);
			$this->load->view('templates/footer', $this->data);
			
		}
		public function armorgt($slug = NULL) {
			$greg = new Greg_model;
			$data['armor'] = $greg->get_armorGT($slug);
			$this->data['title'] = $data['armor']['name'];
			$this->data['name'] = $data['armor']['name'];
			$this->data['img'] = $data['armor']['imgdir'];
			$data['armorcraft'] = $greg->get_craftItemsGT($slug);
			$data['tableinarmorgt'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/armorgt', $data);
			$this->load->view('templates/footer', $this->data);
			
		}

		public function electromachinesgt($slug = NULL) {
			$greg = new Greg_model;
			$data['elecmach'] = $greg->get_electromachinesGT($slug);
			$this->data['title'] = $data['elecmach']['name'];
			$this->data['name'] = $data['elecmach']['name'];
			$this->data['img'] = $data['elecmach']['imgdir'];
			$this->data['desc'] = $data['elecmach']['long_desc'];
			$data['elecmachcraft'] = $greg->get_craftItemsGT($slug);
			$data['elecmachinterface'] = $greg->get_interfaceGT($slug);
			$data['tableinelecmachgt'] = $greg->get_tableInGT($slug);
			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/electromachinesgt', $data);
			$this->load->view('templates/footer', $this->data);
			
		}
		//создание страницы для роторов турбин
		public function rotorGT() {
			$this->data['title'] = "Роторы и их математика";
			$greg = new Greg_model;
			$data['Rotor'] = $greg->get_rotorgt();
			$data['NameRotor'] = $greg->get_materialrotorgt();
			$data['Plasma'] = $greg->get_plasmagt();
			$data['Gas'] = $greg->get_gasgt();

			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/rotorgt', $data);
			$this->load->view('templates/footer', $this->data);
		}

		//создание страницы для роторов турбин
		public function smalloresGT() {
			$this->data['title'] = "Small Ores Gregtech 5";
			$greg = new Greg_model;
			$data['SmallOresGen'] = $greg->get_smallOreGengt();

			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/smalloresgt', $data);
			$this->load->view('templates/footer', $this->data);
		}
		//создание страницы для роторов турбин
		public function oremixveinGT() {
			$this->data['title'] = "Ore Mix Veins Gregtech 5";
			$greg = new Greg_model;
			$data['OreMixvein'] = $greg->get_oreMixVeinGengt();

			$this->load->view('templates/header', $this->data);
			$this->load->view('gregtech/oremixveingt', $data);
			$this->load->view('templates/footer', $this->data);
		}
	}
?>