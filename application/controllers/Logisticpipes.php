<?php
    

    class Logisticpipes extends MY_Controller {

        public function __construct(){
			parent::__construct();
			$this->load->model('Logisticpipes_model');         

        }
        //главная страница LP с папкой и именем файла logisticpipes
        public function logisticpipes() {
            $LPmodel = new Logisticpipes_model;

            $data['itempipe'] = $LPmodel->get_itempipeLP();
            $data['fluidpipe'] = $LPmodel->get_fluidpipeLP();
            $data['itemandtools'] = $LPmodel->get_itemandtoolsLP();
            $data['blocks'] = $LPmodel->get_blocksLP();
            $data['module'] = $LPmodel->get_moduleLP();
            $data['upgrade'] = $LPmodel->get_upgradeLP();

            $this->data['title'] = "Logistic Pipes";
			$this->load->view('templates/header', $this->data);
			$this->load->view('logisticpipes/logisticpipes', $data);
			$this->load->view('templates/footer', $this->data);
        }
        //предметные трубы
        public function itempipeLP($slug = FALSE) {
            $LPmodel = new Logisticpipes_model;

            $data['itempipeLP'] = $LPmodel->get_itempipeLP($slug);
            $this->data['title'] = $data['itempipeLP']['name'];
            $this->data['name'] = $data['itempipeLP']['name'];
			$this->data['img'] = $data['itempipeLP']['imgdir'];
            $this->data['desc'] = $data['itempipeLP']['long_desc'];
            $data['craftitempipe'] = $LPmodel->get_craftItemsLP($slug);
            $data['interitempipe'] = $LPmodel->get_interfaceLP($slug);

            
			$this->load->view('templates/header', $this->data);
			$this->load->view('logisticpipes/itempipeLP', $data);
			$this->load->view('templates/footer', $this->data);   

        }
        //жидскостные трубы
        public function fluidpipeLP($slug = FALSE) {
            $LPmodel = new Logisticpipes_model;

            $data['fluidpipeLP'] = $LPmodel->get_fluidpipeLP($slug);
            $this->data['title'] = $data['fluidpipeLP']['name'];
            $this->data['name'] = $data['fluidpipeLP']['name'];
			$this->data['img'] = $data['fluidpipeLP']['imgdir'];
            $this->data['desc'] = $data['fluidpipeLP']['long_desc'];
            $data['craftfluidpipe'] = $LPmodel->get_craftItemsLP($slug);
            $data['interfluidpipe'] = $LPmodel->get_interfaceLP($slug);

            
			$this->load->view('templates/header', $this->data);
			$this->load->view('logisticpipes/fluidpipeLP', $data);
			$this->load->view('templates/footer', $this->data);   

        }
        //предметы и инструменты
        public function itemandtoolsLP($slug = FALSE) {
            $LPmodel = new Logisticpipes_model;

            $data['itemandtoolsLP'] = $LPmodel->get_itemandtoolsLP($slug);
            $this->data['title'] = $data['itemandtoolsLP']['name'];
            $this->data['name'] = $data['itemandtoolsLP']['name'];
			$this->data['img'] = $data['itemandtoolsLP']['imgdir'];
            $this->data['desc'] = $data['itemandtoolsLP']['long_desc'];
            $data['craftitemandtoolspipe'] = $LPmodel->get_craftItemsLP($slug);
            $data['interitemandtool'] = $LPmodel->get_interfaceLP($slug);

            
			$this->load->view('templates/header', $this->data);
			$this->load->view('logisticpipes/itemandtoolsLP', $data);
			$this->load->view('templates/footer', $this->data);   

        }

        //блоки
        public function blocksLP($slug = FALSE) {
            $LPmodel = new Logisticpipes_model;

            $data['blocksLP'] = $LPmodel->get_blocksLP($slug);
            $this->data['title'] = $data['blocksLP']['name'];
            $this->data['name'] = $data['blocksLP']['name'];
			$this->data['img'] = $data['blocksLP']['imgdir'];
            $this->data['desc'] = $data['blocksLP']['long_desc'];
            $data['craftblocks'] = $LPmodel->get_craftItemsLP($slug);
            $data['interblock'] = $LPmodel->get_interfaceLP($slug);

            
			$this->load->view('templates/header', $this->data);
			$this->load->view('logisticpipes/blocksLP', $data);
			$this->load->view('templates/footer', $this->data);   

        }

        //модули
        public function moduleLP($slug = FALSE) {
            $LPmodel = new Logisticpipes_model;

            $data['moduleLP'] = $LPmodel->get_moduleLP($slug);
            $this->data['title'] = $data['moduleLP']['name'];
            $this->data['name'] = $data['moduleLP']['name'];
			$this->data['img'] = $data['moduleLP']['imgdir'];
            $this->data['desc'] = $data['moduleLP']['long_desc'];
            $data['craftmodule'] = $LPmodel->get_craftItemsLP($slug);
            $data['intermodule'] = $LPmodel->get_interfaceLP($slug);

            
			$this->load->view('templates/header', $this->data);
			$this->load->view('logisticpipes/moduleLP', $data);
			$this->load->view('templates/footer', $this->data);   

        }

        //апгрейды
        public function upgradeLP($slug = FALSE) {
            $LPmodel = new Logisticpipes_model;

            $data['upgradeLP'] = $LPmodel->get_upgradeLP($slug);
            $this->data['title'] = $data['upgradeLP']['name'];
            $this->data['name'] = $data['upgradeLP']['name'];
			$this->data['img'] = $data['upgradeLP']['imgdir'];
            $this->data['desc'] = $data['upgradeLP']['long_desc'];
            $data['craftupgrade'] = $LPmodel->get_craftItemsLP($slug);

            
			$this->load->view('templates/header', $this->data);
			$this->load->view('logisticpipes/upgradeLP', $data);
			$this->load->view('templates/footer', $this->data);   

        }
        
    }

?>