<?php 
	defined('BASEPATH') OR exit('No direc script access allowed');

	/**
	 * 
	 */
	class Main extends MY_Controller {
		
		public function __construct()
		{
			parent::__construct();
			$this->load->model('News_model');
		}
/*Вывод страниц, которые созданы в папке main,
и внести ссылки в header*/


		/*вывод главной страницы*/
		public function index() {
			/* Подгрузка модели новостей, подобие Gregtech макета
			имя массива news для цикла в templates/menu */
			$news = new News_model;
			$data['news'] = $news->getNews();
			/*Заголовок вкладки*/
			$this->data['title'] = "Главная Страница";
			$this->load->view('templates/header', $this->data);
			$this->load->view('main/index', $data);
			$this->load->view('templates/footer', $this->data);
		}
		/*вывод для Вики*/
		public function wiki() {
			$news = new News_model;
			$data['news'] = $news->getNews();
			$this->data['title'] = "Вики";
			$this->load->view('templates/header', $this->data);
			$this->load->view('main/wiki', $data);
			$this->load->view('templates/footer', $this->data);
		}
		/*вывод для Гайдов*/
		public function guide() {
			$news = new News_model;
			$data['news'] = $news->getNews();
			$this->data['title'] = "Гайды";
			$this->load->view('templates/header', $this->data);
			$this->load->view('main/guide', $data);
			$this->load->view('templates/footer', $this->data);
		}
		/*вывод для контактов*/
		public function contacts() {
			$news = new News_model;
			$data['news'] = $news->getNews();
			$this->data['title'] = "Контакты";
			$this->load->view('templates/header', $this->data);
			$this->load->view('main/contacts', $data);
			$this->load->view('templates/footer', $this->data);
		}
	}
 ?>