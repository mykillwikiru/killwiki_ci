<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
/*Подключение страниц для контроллера Main.php*/
$route['wiki'] = 'main/wiki';
$route['guide'] = 'main/guide';
$route['contacts'] = 'main/contacts';
$route['search/(:any)'] = 'search/$1';
/*контроллер загрузки страницы из контроллера Gregtech*/
$route['gregtech'] = 'gregtech/gregtech';
$route['gregtech/rotorgt'] = 'gregtech/rotorgt';
$route['gregtech/smalloresgt'] = 'gregtech/smalloresgt';
$route['gregtech/oremixveingt'] = 'gregtech/oremixveingt';
$route['gregtech/toolsgt/(:any)'] = 'gregtech/toolsgt/$1';
$route['gregtech/advtoolsgt/(:any)'] = 'gregtech/advtoolsgt/$1';
$route['gregtech/electoolsgt/(:any)'] = 'gregtech/electoolsgt/$1';
$route['gregtech/multiblocksgt/(:any)'] = 'gregtech/multiblocksgt/$1';
$route['gregtech/blockformultiblockgt/(:any)'] = 'gregtech/blockformultiblockgt/$1';
$route['gregtech/bronzeagegt/(:any)'] = 'gregtech/bronzeagegt/$1';
$route['gregtech/hpagegt/(:any)'] = 'gregtech/hpagegt/$1';
$route['gregtech/batterygt/(:any)'] = 'gregtech/batterygt/$1';
$route['gregtech/machinecomponentsgt/(:any)'] = 'gregtech/machinecomponentsgt/$1';
$route['gregtech/otheritemsgt/(:any)'] = 'gregtech/otheritemsgt/$1';
$route['gregtech/circuitsgt/(:any)'] = 'gregtech/circuitsgt/$1';
$route['gregtech/utilitmachinesgt/(:any)'] = 'gregtech/utilitmachinesgt/$1';
$route['gregtech/armorgt/(:any)'] = 'gregtech/armorgt/$1';
$route['gregtech/electromachinesgt/(:any)'] = 'gregtech/electromachinesgt/$1';
/*контроллер загрузки страницы из контроллера Embers*/
$route['embers'] = 'embers/embers';
$route['embers/oregenember/(:any)'] = 'embers/oregenember/$1';
$route['embers/toolsember/(:any)'] = 'embers/toolsember/$1';
$route['embers/advtoolsember/(:any)'] = 'embers/advtoolsember/$1';
$route['embers/transportember/(:any)'] = 'embers/transportember/$1';
$route['embers/energycellember/(:any)'] = 'embers/energycellember/$1';
$route['embers/otheritemsember/(:any)'] = 'embers/otheritemsember/$1';
$route['embers/modifiersember/(:any)'] = 'embers/modifiersember/$1';
$route['embers/mainblockember/(:any)'] = 'embers/mainblockember/$1';
$route['embers/alchemyember/(:any)'] = 'embers/alchemyember/$1';
$route['embers/armorember/(:any)'] = 'embers/armorember/$1';
/*контроллер загрузки Logistic Pipes*/
$route['logisticpipes'] = 'logisticpipes/logisticpipes';
$route['logisticpipes/itempipeLP/(:any)'] = 'logisticpipes/itempipeLP/$1';
$route['logisticpipes/fluidpipeLP/(:any)'] = 'logisticpipes/fluidpipeLP/$1';
$route['logisticpipes/itemandtoolsLP/(:any)'] = 'logisticpipes/itemandtoolsLP/$1';
$route['logisticpipes/blocksLP/(:any)'] = 'logisticpipes/blocksLP/$1';
$route['logisticpipes/moduleLP/(:any)'] = 'logisticpipes/moduleLP/$1';
$route['logisticpipes/upgradeLP/(:any)'] = 'logisticpipes/upgradeLP/$1';
/*контроллер загрузки RFTools*/
$route['rftools'] = 'rftools/rftools';
$route['rftools/materialRFT'] = 'rftools/materialRFT/$1';
$route['rftools/toolsRFT'] = 'rftools/toolsRFT/$1';
$route['rftools/smallmachineRFT'] = 'rftools/smallmachineRFT/$1';
$route['rftools/enviromentalmoduleRFT'] = 'rftools/enviromentalmoduleRFT/$1';
$route['rftools/storagemoduleRFT'] = 'rftools/storagemoduleRFT/$1';
$route['rftools/screenmoduleRFT'] = 'rftools/screenmoduleRFT/$1';
$route['rftools/shapecardRFT'] = 'rftools/shapecardRFT/$1';
$route['rftools/machineRFT'] = 'rftools/machineRFT/$1';

$route['gtplusplus'] = 'gtplusplus/gtplusplus';
$route['gtplusplus/multiblockGTPP/(:any)'] = 'gtplusplus/multiblockGTPP/$1';
$route['gtplusplus/toolsGTPP/(:any)'] = 'gtplusplus/toolsGTPP/$1';
$route['gtplusplus/utilblockformultiGTPP/(:any)'] = 'gtplusplus/utilblockformultiGTPP/$1';
$route['gtplusplus/simplemachGTPP/(:any)'] = 'gtplusplus/simplemachGTPP/$1';
$route['gtplusplus/utilmachGTPP/(:any)'] = 'gtplusplus/utilmachGTPP/$1';
$route['gtplusplus/machcompGTPP/(:any)'] = 'gtplusplus/machcompGTPP/$1';

