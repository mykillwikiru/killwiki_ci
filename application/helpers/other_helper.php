<?php 
	 if(!function_exists('show_active_menu')) {

	 	function show_active_menu($slug) {

	 		$ci=& get_instance();
	 		$result = "";
	 		//активация ссылок 1 уровня
	 		if($ci->uri->segment(1, 0) === $slug) {
	 			$result = "class = 'active'";
	 		}
	 		//активация ссылок 2 уровня и выделение вики
	 		if($ci->uri->segment(2, 'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech') {
				$result = 'class="active"';
			}
	 		if($ci->uri->segment(2, 'wiki') === $slug && $ci->uri->segment(1,0) === 'embers') {
				$result = 'class="active"';
			}
	 		if($ci->uri->segment(2, 'wiki') === $slug && $ci->uri->segment(1,0) === 'logisticpipes') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(2, 'wiki') === $slug && $ci->uri->segment(1,0) === 'rftools') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(2, 'wiki') === $slug && $ci->uri->segment(1,0) === 'gtplusplus') {
				$result = 'class="active"';
			}

			 //активация ссылок 4 уровня, при переходе минуя 3 уровни подразделов мода Gregtech
			 //GREGTECH
	 		if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'toolsgt') {
				$result = 'class="active"';
			}
	 		if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'advtoolsgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'electoolsgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'multiblocksgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'blockformultiblockgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'bronzeagegt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'hpagegt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'batterygt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'machinecomponentsgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'otheritemsgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'circuitsgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'utilitmachinesgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'armorgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'electromachinesgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'rotorgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'smalloresgt') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gregtech' && $ci->uri->segment(2,0) === 'oremixveingt') {
				$result = 'class="active"';
			}
			//EMBERS
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'oregenember') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'toolsember') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'advtoolsember') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'transportember') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'energycellember') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'otheritemsember') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'modifiersember') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'mainblockember') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'alchemyember') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'embers' && $ci->uri->segment(2,0) === 'armorember') {
				$result = 'class="active"';
			}
			//LOGISTIC PIPES
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'logisticpipes' && $ci->uri->segment(2,0) === 'itempipeLP') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'logisticpipes' && $ci->uri->segment(2,0) === 'fluidpipeLP') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'logisticpipes' && $ci->uri->segment(2,0) === 'itemandtoolsLP') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'logisticpipes' && $ci->uri->segment(2,0) === 'blocksLP') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'logisticpipes' && $ci->uri->segment(2,0) === 'moduleLP') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'logisticpipes' && $ci->uri->segment(2,0) === 'upgradeLP') {
				$result = 'class="active"';
			}
			//RFTOOLS
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'rftools' && $ci->uri->segment(2,0) === 'materialRFT') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'rftools' && $ci->uri->segment(2,0) === 'toolsRFT') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'rftools' && $ci->uri->segment(2,0) === 'smallmachineRFT') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'rftools' && $ci->uri->segment(2,0) === 'enviromentalmoduleRFT') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'rftools' && $ci->uri->segment(2,0) === 'storagemoduleRFT') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'rftools' && $ci->uri->segment(2,0) === 'screenmoduleRFT') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'rftools' && $ci->uri->segment(2,0) === 'shapecardRFT') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'rftools' && $ci->uri->segment(2,0) === 'machineRFT') {
				$result = 'class="active"';
			}

			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gtplusplus' && $ci->uri->segment(2,0) === 'machcompGTPP') {
				$result = 'class="active"';
			}

			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gtplusplus' && $ci->uri->segment(2,0) === 'multiblockGTPP') {
				$result = 'class="active"';
			}

			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gtplusplus' && $ci->uri->segment(2,0) === 'simplemachGTPP') {
				$result = 'class="active"';
			}

			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gtplusplus' && $ci->uri->segment(2,0) === 'toolsGTPP') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gtplusplus' && $ci->uri->segment(2,0) === 'utilblockformultiGTPP') {
				$result = 'class="active"';
			}
			if($ci->uri->segment(4,'wiki') === $slug && $ci->uri->segment(1,0) === 'gtplusplus' && $ci->uri->segment(2,0) === 'utilmachGTPP') {
				$result = 'class="active"';
			}
			
			

	 		return $result;

	 	}
	 }