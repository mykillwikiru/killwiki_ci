<?php 

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->data['title'] = "KillFindWiki";

		$this->load->model('Greg_model');
		$this->load->model('Embers_model');
		$this->load->model('Logisticpipes_model');
		$this->load->model('Rftools_model');
		$this->load->model('Gtplusplus_model');
		
		$this->data['toolsGT'] = $this->Greg_model->get_toolsGT();
	}

}

?>