document.addEventListener("DOMContentLoaded", function() {
	document.getElementById('click').onclick = function() {
		
	//read value="" in id = calc and make array from string
	var string = document.getElementById('calc').value;
	var between = string.split(',');
//CALC FOR STEAM
	//view everystring
	document.getElementById('steamread1').innerHTML = between[0];
	document.getElementById('steamread2').innerHTML = between[1];
	document.getElementById('steamread3').innerHTML = between[2];
	document.getElementById('steamread4').innerHTML = between[3];
	//math
	var optiflowreg1 = between[0] / 20;
	var optiflowreg2 = between[1] / 20;
	var optiflowreg3 = between[2] / 20;
	var optiflowreg4 = between[3] / 20;
	//print value is selected
	
	document.getElementById('flow1').innerHTML = optiflowreg1;
	document.getElementById('flow2').innerHTML = optiflowreg2;
	document.getElementById('flow3').innerHTML = optiflowreg3;
	document.getElementById('flow4').innerHTML = optiflowreg4;

	//energy amount select steam and rotor
	var steam = document.getElementById('steam').value;
	var amount1 = (steam * optiflowreg1 * between[4]) / 100;
	var amount2 = (steam * optiflowreg2 * between[5]) / 100;
	var amount3 = (steam * optiflowreg3 * between[6]) / 100;
	var amount4 = (steam * optiflowreg4 * between[7]) / 100;
 
	document.getElementById('amounteng1').innerHTML = amount1;
	document.getElementById('amounteng2').innerHTML = amount2;
	document.getElementById('amounteng3').innerHTML = amount3;
	document.getElementById('amounteng4').innerHTML = amount4;

	var tier1 = document.getElementById('tier1');
	
	if (1 < amount1 && amount1 < 32) 
	{
		tier1.innerHTML = "LV";
	} else if (
		33 < amount1 && amount1 < 128) 
	{
		tier1.innerHTML = "MV";
	} else if (
		128 < amount1 && amount1 < 512) 
	{
		tier1.innerHTML = "HV";
	} else if (
		513 < amount1 && amount1 < 2048) 
	{
		tier1.innerHTML = "EV";
	} else if (
		2049 < amount1 && amount1 < 8192) 
	{
		tier1.innerHTML = "IV";
	} else if (
		8193 < amount1 && amount1 < 32768) 
	{
		tier1.innerHTML = "LuV";
	} else if (
		32769 < amount1 && amount1 < 131072) 
	{
		tier1.innerHTML = "ZPM";
	} else if (
		131073 < amount1 && amount1 < 524288) 
	{
		tier1.innerHTML = "UV";
	} else if (
		524289 < amount1 && amount1 < 1000000) 
	{
		tier1.innerHTML = "MAX";
	}
	
	var tier2 = document.getElementById('tier2');
	
	if (1 < amount2 && amount2 < 32) 
	{
		tier2.innerHTML = "LV";
	} else if (
		33 < amount2 && amount2 < 128) 
	{
		tier2.innerHTML = "MV";
	} else if (
		128 < amount2 && amount2 < 512) 
	{
		tier2.innerHTML = "HV";
	} else if (
		513 < amount2 && amount2 < 2048) 
	{
		tier2.innerHTML = "EV";
	} else if (
		2049 < amount2 && amount2 < 8192) 
	{
		tier2.innerHTML = "IV";
	} else if (
		8193 < amount2 && amount2 < 32768) 
	{
		tier2.innerHTML = "LuV";
	} else if (
		32769 < amount2 && amount2 < 131072) 
	{
		tier2.innerHTML = "ZPM";
	} else if (
		131073 < amount2 && amount2 < 524288) 
	{
		tier2.innerHTML = "UV";
	} else if (
		524289 < amount2 && amount2 < 1000000) 
	{
		tier2.innerHTML = "MAX";
	}

	var tier3 = document.getElementById('tier3');
	
	if (1 < amount3 && amount3 < 32) 
	{
		tier3.innerHTML = "LV";
	} else if (
		33 < amount3 && amount3 < 128) 
	{
		tier3.innerHTML = "MV";
	} else if (
		128 < amount3 && amount3 < 512) 
	{
		tier3.innerHTML = "HV";
	} else if (
		513 < amount3 && amount3 < 2048) 
	{
		tier3.innerHTML = "EV";
	} else if (
		2049 < amount3 && amount3 < 8192) 
	{
		tier3.innerHTML = "IV";
	} else if (
		8193 < amount3 && amount3 < 32768) 
	{
		tier3.innerHTML = "LuV";
	} else if (
		32769 < amount3 && amount3 < 131072) 
	{
		tier3.innerHTML = "ZPM";
	} else if (
		131073 < amount3 && amount3 < 524288) 
	{
		tier3.innerHTML = "UV";
	} else if (
		524289 < amount3 && amount3 < 1000000) 
	{
		tier3.innerHTML = "MAX";
	}

	var tier4 = document.getElementById('tier4');
	
	if (1 < amount4 && amount4 < 32) 
	{
		tier4.innerHTML = "LV";
	} else if (
		33 < amount4 && amount4 < 128) 
	{
		tier4.innerHTML = "MV";
	} else if (
		128 < amount4 && amount4 < 512) 
	{
		tier4.innerHTML = "HV";
	} else if (
		513 < amount4 && amount4 < 2048) 
	{
		tier4.innerHTML = "EV";
	} else if (
		2049 < amount4 && amount4 < 8192) 
	{
		tier4.innerHTML = "IV";
	} else if (
		8193 < amount4 && amount4 < 32768) 
	{
		tier4.innerHTML = "LuV";
	} else if (
		32769 < amount4 && amount4 < 131072) 
	{
		tier4.innerHTML = "ZPM";
	} else if (
		131073 < amount4 && amount4 < 524288) 
	{
		tier4.innerHTML = "UV";
	} else if (
		524289 < amount4 && amount4 < 1000000) 
	{
		tier4.innerHTML = "MAX";
	}
//CALC FOR GAS
	//optiflow gas not selected gas
	var gasflow1 = between[0] / 20;
	var gasflow2 = between[1] / 20;
	var gasflow3 = between[2] / 20;
	var gasflow4 = between[3] / 20;

	document.getElementById('gasread1').innerHTML = gasflow1;
	document.getElementById('gasread2').innerHTML = gasflow2;
	document.getElementById('gasread3').innerHTML = gasflow3;
	document.getElementById('gasread4').innerHTML = gasflow4;

	// optimalflow with selected gas
	var gas = document.getElementById('gas').value;
	var flowWithGas1 = gasflow1 / gas;
	var flowWithGas2 = gasflow2 / gas;
	var flowWithGas3 = gasflow3 / gas;
	var flowWithGas4 = gasflow4 / gas;

	document.getElementById('gasflow1').innerHTML = flowWithGas1;
	document.getElementById('gasflow2').innerHTML = flowWithGas2;
	document.getElementById('gasflow3').innerHTML = flowWithGas3;
	document.getElementById('gasflow4').innerHTML = flowWithGas4;

	//energy amount select gas and rotor
	var gasAmount1 = (gas * flowWithGas1 * between[4]) / 100;
	var gasAmount2 = (gas * flowWithGas2 * between[5]) / 100;
	var gasAmount3 = (gas * flowWithGas3 * between[6]) / 100;
	var gasAmount4 = (gas * flowWithGas4 * between[7]) / 100;
	
	document.getElementById('gasamount1').innerHTML = gasAmount1;
	document.getElementById('gasamount2').innerHTML = gasAmount2;
	document.getElementById('gasamount3').innerHTML = gasAmount3;
	document.getElementById('gasamount4').innerHTML = gasAmount4;

	var gastier1 = document.getElementById('gastier1');
	
	if (1 < gasAmount1 && gasAmount1 < 32) 
	{
		gastier1.innerHTML = "LV";
	} else if (
		33 < gasAmount1 && gasAmount1 < 128) 
	{
		gastier1.innerHTML = "MV";
	} else if (
		128 < gasAmount1 && gasAmount1 < 512) 
	{
		gastier1.innerHTML = "HV";
	} else if (
		513 < gasAmount1 && gasAmount1 < 2048) 
	{
		gastier1.innerHTML = "EV";
	} else if (
		2049 < gasAmount1 && gasAmount1 < 8192) 
	{
		gastier1.innerHTML = "IV";
	} else if (
		8193 < gasAmount1 && gasAmount1 < 32768) 
	{
		gastier1.innerHTML = "LuV";
	} else if (
		32769 < gasAmount1 && gasAmount1 < 131072) 
	{
		gastier1.innerHTML = "ZPM";
	} else if (
		131073 < gasAmount1 && gasAmount1 < 524288) 
	{
		gastier1.innerHTML = "UV";
	} else if (
		524289 < gasAmount1 && gasAmount1 < 1000000) 
	{
		gastier1.innerHTML = "MAX";
	}
	
	var gastier2 = document.getElementById('gastier2');
	
	if (1 < gasAmount2 && gasAmount2 < 32) 
	{
		gastier2.innerHTML = "LV";
	} else if (
		33 < gasAmount2 && gasAmount2 < 128) 
	{
		gastier2.innerHTML = "MV";
	} else if (
		128 < gasAmount2 && gasAmount2 < 512) 
	{
		gastier2.innerHTML = "HV";
	} else if (
		513 < gasAmount2 && gasAmount2 < 2048) 
	{
		gastier2.innerHTML = "EV";
	} else if (
		2049 < gasAmount2 && gasAmount2 < 8192) 
	{
		gastier2.innerHTML = "IV";
	} else if (
		8193 < gasAmount2 && gasAmount2 < 32768) 
	{
		gastier2.innerHTML = "LuV";
	} else if (
		32769 < gasAmount2 && gasAmount2 < 131072) 
	{
		gastier2.innerHTML = "ZPM";
	} else if (
		131073 < gasAmount2 && gasAmount2 < 524288) 
	{
		gastier2.innerHTML = "UV";
	} else if (
		524289 < gasAmount2 && gasAmount2 < 1000000) 
	{
		gastier2.innerHTML = "MAX";
	}

	var gastier3 = document.getElementById('gastier3');
	
	if (1 < gasAmount3 && gasAmount3 < 32) 
	{
		gastier3.innerHTML = "LV";
	} else if (
		33 < gasAmount3 && gasAmount3 < 128) 
	{
		gastier3.innerHTML = "MV";
	} else if (
		128 < gasAmount3 && gasAmount3 < 512) 
	{
		gastier3.innerHTML = "HV";
	} else if (
		513 < gasAmount3 && gasAmount3 < 2048) 
	{
		gastier3.innerHTML = "EV";
	} else if (
		2049 < gasAmount3 && gasAmount3 < 8192) 
	{
		gastier3.innerHTML = "IV";
	} else if (
		8193 < gasAmount3 && gasAmount3 < 32768) 
	{
		gastier3.innerHTML = "LuV";
	} else if (
		32769 < gasAmount3 && gasAmount3 < 131072) 
	{
		gastier3.innerHTML = "ZPM";
	} else if (
		131073 < gasAmount3 && gasAmount3 < 524288) 
	{
		gastier3.innerHTML = "UV";
	} else if (
		524289 < gasAmount3 && gasAmount3 < 1000000) 
	{
		gastier3.innerHTML = "MAX";
	}

	var gastier4 = document.getElementById('gastier4');
	
	if (1 < gasAmount4 && gasAmount4 < 32) 
	{
		gastier4.innerHTML = "LV";
	} else if (
		33 < gasAmount4 && gasAmount4 < 128) 
	{
		gastier4.innerHTML = "MV";
	} else if (
		128 < gasAmount4 && gasAmount4 < 512) 
	{
		gastier4.innerHTML = "HV";
	} else if (
		513 < gasAmount4 && gasAmount4 < 2048) 
	{
		gastier4.innerHTML = "EV";
	} else if (
		2049 < gasAmount4 && gasAmount4 < 8192) 
	{
		gastier4.innerHTML = "IV";
	} else if (
		8193 < gasAmount4 && gasAmount4 < 32768) 
	{
		gastier4.innerHTML = "LuV";
	} else if (
		32769 < gasAmount4 && gasAmount4 < 131072) 
	{
		gastier4.innerHTML = "ZPM";
	} else if (
		131073 < gasAmount4 && gasAmount4 < 524288) 
	{
		gastier4.innerHTML = "UV";
	} else if (
		524289 < gasAmount4 && gasAmount4 < 1000000) 
	{
		gastier4.innerHTML = "MAX";
	}
//CALC FOR PLASMA
	//optiflow plasma not selected plasma
	var plasmaflow1 = between[0] * 2;
	var plasmaflow2 = between[1] * 2;
	var plasmaflow3 = between[2] * 2;
	var plasmaflow4 = between[3] * 2;

	document.getElementById('plasmaread1').innerHTML = plasmaflow1;
	document.getElementById('plasmaread2').innerHTML = plasmaflow2;
	document.getElementById('plasmaread3').innerHTML = plasmaflow3;
	document.getElementById('plasmaread4').innerHTML = plasmaflow4;

	// optimalflow with selected plasma
	var plasma = document.getElementById('plasma').value;
	var flowWithplasma1 = plasmaflow1 / plasma;
	var flowWithplasma2 = plasmaflow2 / plasma;
	var flowWithplasma3 = plasmaflow3 / plasma;
	var flowWithplasma4 = plasmaflow4 / plasma;

	document.getElementById('plasmaflow1').innerHTML = flowWithplasma1;
	document.getElementById('plasmaflow2').innerHTML = flowWithplasma2;
	document.getElementById('plasmaflow3').innerHTML = flowWithplasma3;
	document.getElementById('plasmaflow4').innerHTML = flowWithplasma4;

	//energy amount select plasma and rotor
	var plasmaAmount1 = (plasma * flowWithplasma1 * between[4]) / 100;
	var plasmaAmount2 = (plasma * flowWithplasma2 * between[5]) / 100;
	var plasmaAmount3 = (plasma * flowWithplasma3 * between[6]) / 100;
	var plasmaAmount4 = (plasma * flowWithplasma4 * between[7]) / 100;
	
	document.getElementById('plasmaamount1').innerHTML = plasmaAmount1;
	document.getElementById('plasmaamount2').innerHTML = plasmaAmount2;
	document.getElementById('plasmaamount3').innerHTML = plasmaAmount3;
	document.getElementById('plasmaamount4').innerHTML = plasmaAmount4;

	var plasmatier1 = document.getElementById('plasmatier1');
	
	if (1 < plasmaAmount1 && plasmaAmount1 < 32) 
	{
		plasmatier1.innerHTML = "LV";
	} else if (
		33 < plasmaAmount1 && plasmaAmount1 < 128) 
	{
		plasmatier1.innerHTML = "MV";
	} else if (
		128 < plasmaAmount1 && plasmaAmount1 < 512) 
	{
		plasmatier1.innerHTML = "HV";
	} else if (
		513 < plasmaAmount1 && plasmaAmount1 < 2048) 
	{
		plasmatier1.innerHTML = "EV";
	} else if (
		2049 < plasmaAmount1 && plasmaAmount1 < 8192) 
	{
		plasmatier1.innerHTML = "IV";
	} else if (
		8193 < plasmaAmount1 && plasmaAmount1 < 32768) 
	{
		plasmatier1.innerHTML = "LuV";
	} else if (
		32769 < plasmaAmount1 && plasmaAmount1 < 131072) 
	{
		plasmatier1.innerHTML = "ZPM";
	} else if (
		131073 < plasmaAmount1 && plasmaAmount1 < 524288) 
	{
		plasmatier1.innerHTML = "UV";
	} else if (
		524289 < plasmaAmount1 && plasmaAmount1 < 1000000) 
	{
		plasmatier1.innerHTML = "MAX";
	}
	
	var plasmatier2 = document.getElementById('plasmatier2');
	
	if (1 < plasmaAmount2 && plasmaAmount2 < 32) 
	{
		plasmatier2.innerHTML = "LV";
	} else if (
		33 < plasmaAmount2 && plasmaAmount2 < 128) 
	{
		plasmatier2.innerHTML = "MV";
	} else if (
		128 < plasmaAmount2 && plasmaAmount2 < 512) 
	{
		plasmatier2.innerHTML = "HV";
	} else if (
		513 < plasmaAmount2 && plasmaAmount2 < 2048) 
	{
		plasmatier2.innerHTML = "EV";
	} else if (
		2049 < plasmaAmount2 && plasmaAmount2 < 8192) 
	{
		plasmatier2.innerHTML = "IV";
	} else if (
		8193 < plasmaAmount2 && plasmaAmount2 < 32768) 
	{
		plasmatier2.innerHTML = "LuV";
	} else if (
		32769 < plasmaAmount2 && plasmaAmount2 < 131072) 
	{
		plasmatier2.innerHTML = "ZPM";
	} else if (
		131073 < plasmaAmount2 && plasmaAmount2 < 524288) 
	{
		plasmatier2.innerHTML = "UV";
	} else if (
		524289 < plasmaAmount2 && plasmaAmount2 < 1000000) 
	{
		plasmatier2.innerHTML = "MAX";
	}

	var plasmatier3 = document.getElementById('plasmatier3');
	
	if (1 < plasmaAmount3 && plasmaAmount3 < 32) 
	{
		plasmatier3.innerHTML = "LV";
	} else if (
		33 < plasmaAmount3 && plasmaAmount3 < 128) 
	{
		plasmatier3.innerHTML = "MV";
	} else if (
		128 < plasmaAmount3 && plasmaAmount3 < 512) 
	{
		plasmatier3.innerHTML = "HV";
	} else if (
		513 < plasmaAmount3 && plasmaAmount3 < 2048) 
	{
		plasmatier3.innerHTML = "EV";
	} else if (
		2049 < plasmaAmount3 && plasmaAmount3 < 8192) 
	{
		plasmatier3.innerHTML = "IV";
	} else if (
		8193 < plasmaAmount3 && plasmaAmount3 < 32768) 
	{
		plasmatier3.innerHTML = "LuV";
	} else if (
		32769 < plasmaAmount3 && plasmaAmount3 < 131072) 
	{
		plasmatier3.innerHTML = "ZPM";
	} else if (
		131073 < plasmaAmount3 && plasmaAmount3 < 524288) 
	{
		plasmatier3.innerHTML = "UV";
	} else if (
		524289 < plasmaAmount3 && plasmaAmount3 < 1000000) 
	{
		plasmatier3.innerHTML = "MAX";
	}

	var plasmatier4 = document.getElementById('plasmatier4');
	
	if (1 < plasmaAmount4 && plasmaAmount4 < 32) 
	{
		plasmatier4.innerHTML = "LV";
	} else if (
		33 < plasmaAmount4 && plasmaAmount4 < 128) 
	{
		plasmatier4.innerHTML = "MV";
	} else if (
		128 < plasmaAmount4 && plasmaAmount4 < 512) 
	{
		plasmatier4.innerHTML = "HV";
	} else if (
		513 < plasmaAmount4 && plasmaAmount4 < 2048) 
	{
		plasmatier4.innerHTML = "EV";
	} else if (
		2049 < plasmaAmount4 && plasmaAmount4 < 8192) 
	{
		plasmatier4.innerHTML = "IV";
	} else if (
		8193 < plasmaAmount4 && plasmaAmount4 < 32768) 
	{
		plasmatier4.innerHTML = "LuV";
	} else if (
		32769 < plasmaAmount4 && plasmaAmount4 < 131072) 
	{
		plasmatier4.innerHTML = "ZPM";
	} else if (
		131073 < plasmaAmount4 && plasmaAmount4 < 524288) 
	{
		plasmatier4.innerHTML = "UV";
	} else if (
		524289 < plasmaAmount4 && plasmaAmount4 < 1000000) 
	{
		plasmatier4.innerHTML = "MAX";
	}
		

    	
	};	
});
